/*
    xbj - Java access to dBase files
    Copyright 2023 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package filetest;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.fields.*;
import com.finforecaster.xbj.file.AbstractDBFTestTable;
import com.finforecaster.xbj.file.DBFTableFile;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadExitingDbfTest extends AbstractDBFTestTable {
    @Test
    public void testReadXBJ() throws IOException, XBJException {
        String dbfFileName = getTempFileName("test", ".dbf");
        String ndxFileName0 = getTempFileName("test", ".ndx");
        String ndxFileName1 = getTempFileName("test", ".ndx");

        Files.copy(getClass().getResourceAsStream("/testfiles/external/xbj/class.dbf"), Path.of(dbfFileName));
        Files.copy(getClass().getResourceAsStream("/testfiles/external/xbj/classId.ndx"), Path.of(ndxFileName0));
        Files.copy(getClass().getResourceAsStream("/testfiles/external/xbj/TchrClass.ndx"), Path.of(ndxFileName1));

        DBFTable dbfTable = new DBFTableFile(dbfFileName);
        dbfTable.useIndex(ndxFileName0);
        dbfTable.useIndex(ndxFileName1);

        assertEquals(DBFType.DBASEIII, dbfTable.getDbfHeader().getVersion());
        assertEquals(3, dbfTable.getRecordsCount());
        assertEquals(2, dbfTable.getIndexCount());

        assertFields(dbfTable);
        assertRecords(dbfTable);
        dbfTable.close();
    }

    @Test
    public void testReadDBase5() throws IOException, XBJException {
        String dbfFileName = getTempFileName("test", ".dbf");

        Files.copy(getClass().getResourceAsStream("/testfiles/external/dbase5/CLASS.DBF"), Path.of(dbfFileName));

        DBFTable dbfTable = new DBFTableFile(dbfFileName);
        assertEquals(DBFType.DBASEIII, dbfTable.getDbfHeader().getVersion());
        assertEquals(3, dbfTable.getRecordsCount());

        assertFields(dbfTable);
        assertRecords(dbfTable);
        dbfTable.close();
    }

    @Test
    public void testReadDBaseIII() throws IOException, XBJException {
        String dbfFileName = getTempFileName("test", ".dbf");

        Files.copy(getClass().getResourceAsStream("/testfiles/external/dbaseIII/CLASS.DBF"), Path.of(dbfFileName));

        DBFTable dbfTable = new DBFTableFile(dbfFileName);
        assertEquals(DBFType.DBASEIII, dbfTable.getDbfHeader().getVersion());
        assertEquals(3, dbfTable.getRecordsCount());

        assertFields(dbfTable);
        assertRecords(dbfTable);
        dbfTable.close();
    }

    @Test
    public void testReadDBaseIV() throws IOException, XBJException {
        String dbfFileName = getTempFileName("test", ".dbf");

        Files.copy(getClass().getResourceAsStream("/testfiles/external/dbaseIV/CLASS.DBF"), Path.of(dbfFileName));

        DBFTable dbfTable = new DBFTableFile(dbfFileName);
        assertEquals(DBFType.DBASEIV, dbfTable.getDbfHeader().getVersion());
        assertEquals(3, dbfTable.getRecordsCount());

        assertFields(dbfTable);
        assertRecords(dbfTable);
        dbfTable.close();
    }

    private void assertFields(DBFTable dbfTable) throws XBJException {
        assertField(dbfTable.getField(1), "CLASSID", 'C', 9, 0, CharField.class);
        assertField(dbfTable.getField(2), "CLASSNAME", 'C', 25, 0, CharField.class);
        assertField(dbfTable.getField(3), "TEACHERID", 'C', 9, 0, CharField.class);
        assertField(dbfTable.getField(4), "DAYSMEET", 'C', 7, 0, CharField.class);
        assertField(dbfTable.getField(5), "TIMEMEET", 'C', 4, 0, CharField.class);
        assertField(dbfTable.getField(6), "CREDITS", 'N', 2, 0, NumField.class);
        assertField(dbfTable.getField(7), "UNDERGRAD", 'L', 1, 0, LogicalField.class);
        assertField(dbfTable.getField(8), "DATE", 'D', 8, 0, DateField.class);
    }

    private void assertRecords(DBFTable dbfTable) throws IOException, XBJException {
        assertRecord(dbfTable, "JAVA10100", "Introduction to JAVA     ", "120120120", "NYNYNYN", "0800", " 3", "T", "19750308");
        assertRecord(dbfTable, "JAVA10200", "Intermediate JAVA        ", "300020000", "NYNYNYN", "0930", " 3", "T", "20171101");
        assertRecord(dbfTable, "JAVA501  ", "JAVA And Abstract Algebra", "120120120", "NNYNYNN", "0930", " 6", "F", "20231221");
    }

    private void assertField(Field field, String name, char type, int length, int decPos, Class<? extends Field> fieldClass) {
        assertEquals(name, field.getName());
        assertEquals(type, field.getType());
        assertEquals(length, field.getLength());
        assertEquals(decPos, field.getDecimalPosition());
        assertEquals(fieldClass, field.getClass());
    }

    private void assertRecord(DBFTable dbfTable, String classIdValue, String classNameValue, String teacherIdValue, String daysMeetValue,
                              String timeMeetValue, String creditsValue, String underGradValue, String dateValue) throws IOException, XBJException {
        dbfTable.read();
        assertEquals(dbfTable.getField(1).get(), classIdValue);
        assertEquals(dbfTable.getField(2).get(), classNameValue);
        assertEquals(dbfTable.getField(3).get(), teacherIdValue);
        assertEquals(dbfTable.getField(4).get(), daysMeetValue);
        assertEquals(dbfTable.getField(5).get(), timeMeetValue);
        assertEquals(dbfTable.getField(6).get(), creditsValue);
        assertEquals(dbfTable.getField(7).get(), underGradValue);
        assertEquals(dbfTable.getField(8).get(), dateValue);
    }
}
