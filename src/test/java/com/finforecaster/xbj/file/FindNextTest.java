package com.finforecaster.xbj.file;

import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.NumField;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FindNextTest extends AbstractDBFTestTable {
    @Test
    public void test() throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");
        String ndxFileName = getTempFileName("test", ".ndx");

        DBFTableFile dbfTable = new DBFTableFile();
        CharField keyField = new CharField("KEY", 3);
        NumField numField = new NumField("NUM", 2, 0);
        dbfTable.addField(keyField);
        dbfTable.addField(numField);

        dbfTable.create(dbfFileName);
        dbfTable.createIndex(ndxFileName, "KEY", true, false);

        keyField.put("AAA");
        numField.put(1);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(2);
        dbfTable.write();
        keyField.put("BBB");
        numField.put(3);
        dbfTable.write();
        keyField.put("CCC");
        numField.put(4);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(5);
        dbfTable.write();
        keyField.put("BBB");
        numField.put(6);
        dbfTable.write();
        keyField.put("BBB");
        numField.put(7);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(8);
        dbfTable.write();
        keyField.put("CCC");
        numField.put(9);
        dbfTable.write();
        keyField.put("CCC");
        numField.put(10);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(11);
        dbfTable.write();
        keyField.put("BBB");
        numField.put(12);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(13);
        dbfTable.write();
        keyField.put("CCC");
        numField.put(14);
        dbfTable.write();
        keyField.put("AAA");
        numField.put(15);
        dbfTable.write();

        assertEquals(15, dbfTable.getRecordsCount());

        dbfTable.useIndex(dbfTable.getIndex(1));
        List<Integer> result = new ArrayList<>();

        dbfTable.gotoFirstRecord();
        if (dbfTable.find("BBB")) {
            do {
                result.add(Integer.parseInt(numField.get().trim()));
            } while (dbfTable.findNext());
        }
        assertEquals(4, result.size());
        assertTrue(result.containsAll(Set.of(3, 6, 7, 12)));
        dbfTable.close();
    }
}
