/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.fields.CharField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClipperLargeCharFieldTest extends AbstractDBFTestTable {
    private static final String FIELD_NAME_SHORT = "short";
    private static final String FIELD_NAME_LONG = "long";

    @Test
    public void testBuildNew() {
        final String dbfFileName = getTempFileName("balsetIII", ".dbf");

        CharField scf = new CharField(FIELD_NAME_SHORT, 10);
        CharField lcf = new CharField(FIELD_NAME_LONG, 510);
        DBFTableFile newone = new DBFTableFile();
        newone.addField(scf);
        newone.addField(lcf);
        newone.create(dbfFileName);
        newone.close();

        newone = new DBFTableFile();
        newone.open(dbfFileName);
        assertEquals(2, newone.getFieldCount());

        assertEquals(10, newone.getField(FIELD_NAME_SHORT).getLength());
        assertEquals('C', newone.getField(FIELD_NAME_SHORT).getType());

        assertEquals(510, newone.getField(FIELD_NAME_LONG).getLength());
        assertEquals('C', newone.getField(FIELD_NAME_LONG).getType());

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 510; i++) {
            sb.append('s');
        }
        newone.getField(FIELD_NAME_LONG).put(sb.toString());
        newone.write();
        newone.close();

        newone = new DBFTableFile();
        newone.open(dbfFileName);
        newone.read();
        assertEquals(510, newone.getField(FIELD_NAME_LONG).get().length());
        newone.close();
    }
}
