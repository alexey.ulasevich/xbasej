/*
    xbj - Java access to dBase files
    Copyright 2024 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.MemoField;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class DBFTableFileTest extends AbstractDBFTestTable {
    @Test
    public void testCreateNoFields() throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");

        DBFTableFile dbfTableFile = new DBFTableFile();
        dbfTableFile.create(dbfFileName);

        assertEquals(32 + 1, dbfTableFile.getRaDbfFile().length());
        assertEquals(32 + 1, dbfTableFile.getDbfHeader().getLengthOfHeader());
        assertEquals(0, dbfTableFile.getDbfHeader().getNumberOfRecords());
        assertEquals(1, dbfTableFile.getDbfHeader().getLengthOfRecord());

        dbfTableFile.close();
    }

    @Test
    public void testCreateAddFieldsNoData() throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");

        DBFTableFile dbfTableFile = new DBFTableFile();
        dbfTableFile.addField(new CharField("FLD1", 11));
        dbfTableFile.addField(new CharField("FLD2", 23));
        dbfTableFile.create(dbfFileName);

        assertEquals(32 + 2 * 32 + 1, dbfTableFile.getRaDbfFile().length());
        assertEquals(32 + 2 * 32 + 1, dbfTableFile.getDbfHeader().getLengthOfHeader());
        assertEquals(0, dbfTableFile.getDbfHeader().getNumberOfRecords());
        assertEquals(23 + 11 + 1, dbfTableFile.getDbfHeader().getLengthOfRecord());

        dbfTableFile.close();
    }

    @Test
    public void testCreateAddFieldsMemoNoData() throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");

        DBFTableFile dbfTableFile = new DBFTableFile();
        dbfTableFile.getDbfHeader().setVersion(DBFType.DBASEIII_WITH_MEMO);
        dbfTableFile.addField(new CharField("FLD1", 23));
        dbfTableFile.addField(new MemoField("FLD2"));
        dbfTableFile.create(dbfFileName);

        assertEquals(32 + 2 * 32 + 1, dbfTableFile.getRaDbfFile().length());
        assertEquals(32 + 2 * 32 + 1, dbfTableFile.getDbfHeader().getLengthOfHeader());
        assertEquals(0, dbfTableFile.getDbfHeader().getNumberOfRecords());
        assertEquals(23 + 10 + 1, dbfTableFile.getDbfHeader().getLengthOfRecord());

        assertEquals(512, dbfTableFile.getMemoFile().getRaFile().length());

        dbfTableFile.close();
    }

    @Test
    public void testCreateCloseOpen() {
        String dbfFileName = getTempFileName("test", ".dbf");

        DBFTableFile dbfTableFile = new DBFTableFile();
        dbfTableFile.addField(new CharField("FLD1", 23));
        dbfTableFile.create(dbfFileName);

        dbfTableFile.getField("FLD1").put("REC1");
        dbfTableFile.write();

        dbfTableFile.close();

        dbfTableFile.open(dbfFileName);
        dbfTableFile.gotoRecord(1);
        dbfTableFile.close();
    }
}
