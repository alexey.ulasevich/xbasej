package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.MemoField;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class TestMemoFileLost extends AbstractDBFTestTable {
    public static Stream<Arguments> getMemoVersions() {
        return DBFType.SUPPORTED_MEMO_VERSIONS.stream().map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("getMemoVersions")
    public void testCreateDBF(byte dbfType) throws IOException {
        final String dbfFileName = getTempFileName("temp", ".dbf");

        DBFTableFile d1 = new DBFTableFile();
        d1.getDbfHeader().setVersion(dbfType);

        CharField c3 = new CharField("C3", 10);
        d1.addField(c3);

        CharField c33 = new CharField("C33", 10);
        d1.addField(c33);

        MemoField m333 = new MemoField("m333");
        d1.addField(m333);

        MemoField m3333 = new MemoField("c3333");
        d1.addField(m3333);

        d1.create(dbfFileName);

        m333.put("firstone");
        m3333.put("secondone");
        d1.write();

        Path pathMemo = d1.getMemoFile().getFile().toPath();

        d1.close();

        Files.delete(pathMemo);
        assertFalse(Files.exists(pathMemo));

        XBJException exception = assertThrows(XBJException.class, () -> new DBFTableFile(dbfFileName));
        assertTrue(exception.getMessage().contains(pathMemo.toString()));
    }
}
