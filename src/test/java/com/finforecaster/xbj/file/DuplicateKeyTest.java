/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
 * is there a known bug regarding find_rec(NodeKey, Node, int) in NDX.java
 * while updating a record with non-unique index?
 * In my oppinion this line:
 * for (aNode.set_pos(0); aNode.get_pos() < until && stat > 0; aNode.pos_up())
 * (see full method attached)
 * is handling "stat" not appropiately.
 * When stat is 0, but the record found is not the one we are searching for, there is no loop!
 * We will find the record we are looking for if this line goes like:
 * for (aNode.set_pos(0); aNode.get_pos() < until && stat >= 0; aNode.pos_up())
 * Result: A new entry is being added even if there IS a matching record (=> duplicate entries)!
 */
package com.finforecaster.xbj.file;

import org.junit.jupiter.api.Test;
import com.finforecaster.xbj.fields.CharField;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author joe
 */
public class DuplicateKeyTest extends AbstractDBFTestTable {
    private static final String FIRST_FIELD_NAME = "first";
    private static final String SECOND_FIELD_NAME = "second";

    @Test
    public void test() throws Exception {
        final String dbfFileName = getTempFileName("testupdidx", ".dbf");
        final String ndxFileName = getTempFileName("testupdidx", ".ndx");

        DBFTableFile db = new DBFTableFile();
        CharField c1 = new CharField(FIRST_FIELD_NAME, 2);
        CharField c2 = new CharField(SECOND_FIELD_NAME, 20);
        db.addFields(Set.of(c1, c2));
        db.create(dbfFileName);
        db.createIndex(ndxFileName, FIRST_FIELD_NAME, false);
        db.close();

        db = new DBFTableFile(dbfFileName);
        db.useIndex(ndxFileName);
        c1 = db.getField(FIRST_FIELD_NAME);
        c2 = db.getField(SECOND_FIELD_NAME);
        c1.put("11");
        c2.put("first");
        db.write();
        c1.put("11");
        c2.put("second");
        db.write();
        c1.put("11");
        c2.put("third");
        db.write(); // all three rows have the same value
        db.startTop();
        ArrayList<String> secondValues = new ArrayList<String>();
        for (int lp = 0; lp < 3; lp++) {
            db.findNext();
            secondValues.add(c2.get());
        }
        db.close();

        db = new DBFTableFile(dbfFileName);
        c1 = db.getField(FIRST_FIELD_NAME);
        c2 = db.getField(SECOND_FIELD_NAME);
        db.useIndex(ndxFileName);
        db.find("11");
        int foundCnt = 0;
        HashSet<String> secondVHash = new HashSet<>(secondValues);
        for (int lp = 0; lp < 3; lp++) {
            if (secondVHash.contains(c2.get())) {
                if (c2.get().equals("second")) {
                    c2.put("2nd updated");
                    db.update();
                }

                secondVHash.remove(c2.get());
                foundCnt++;
            } else {
                fail("can't find " + c2.get());
            }
            if (lp < 2)
                db.findNext();
        }
        assertEquals(3, foundCnt);
        secondValues = new ArrayList<>();
        db.startTop();
        for (int lp = 0; lp < 3; lp++) {
            System.out.println(c2.get());
            secondValues.add(c2.get());
            if (lp < 2)
                db.findNext();
        }
        db.close();

        secondVHash = new HashSet<>(secondValues);
        db = new DBFTableFile(dbfFileName);
        assertEquals(3, db.getRecordsCount());
        c1 = db.getField(FIRST_FIELD_NAME);
        c2 = db.getField(SECOND_FIELD_NAME);
        db.useIndex(ndxFileName);
        foundCnt = 0;
        db.find("11");

        for (int lp = 0; lp < 3; lp++) {
            if (secondVHash.contains(c2.get())) {
                secondVHash.remove(c2.get());
                foundCnt++;
            } else {
                fail("can't find " + c2.get());
            }
            if (lp < 2)
                db.findNext();
        }
        assertEquals(3, foundCnt);
        db.close();
    }
}
