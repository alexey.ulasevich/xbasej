/*
    xbj - Java access to dBase files
    Copyright 2023 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import org.junit.jupiter.api.AfterEach;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public abstract class AbstractDBFTestTable {
    private Set<String> tempFiles = new HashSet<>();

    public String getTempFileName(String prefix, String suffix) {
        String tempFileName = System.getProperty("java.io.tmpdir") + "/" + prefix + UUID.randomUUID().toString().replace("-", "") + suffix;
        tempFiles.add(tempFileName);
        return tempFileName;
    }

    @AfterEach
    public void done() {
        for (String tempFileName : tempFiles) {
            try {
                Files.deleteIfExists(new File(tempFileName).toPath());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        tempFiles.clear();
    }
}
