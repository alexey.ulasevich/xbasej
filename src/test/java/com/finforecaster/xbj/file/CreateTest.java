/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.MemoField;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateTest extends AbstractDBFTestTable {

    public static Stream<Arguments> getMemoVersions() {
        return DBFType.SUPPORTED_MEMO_VERSIONS.stream().map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("getMemoVersions")
    public void testCreateDBF(byte dbfType) {
        final String dbfFileName = getTempFileName("temp", ".dbf");

        DBFTableFile d1 = new DBFTableFile();
        d1.getDbfHeader().setVersion(dbfType);

        CharField c3 = new CharField("C3", 10);
        d1.addField(c3);

        CharField c33 = new CharField("C33", 10);
        d1.addField(c33);

        MemoField m333 = new MemoField("m333");
        d1.addField(m333);

        MemoField m3333 = new MemoField("c3333");
        d1.addField(m3333);

        d1.create(dbfFileName);

        m333.put("firstone");
        m3333.put("secondone");
        d1.write();

        d1.gotoRecord(1);

        assertEquals("firstone", d1.getField("m333").get());
        assertEquals("secondone", d1.getField("c3333").get());

        d1.close();
    }
}
