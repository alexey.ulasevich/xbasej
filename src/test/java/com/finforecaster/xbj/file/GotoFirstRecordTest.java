/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.fields.CharField;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GotoFirstRecordTest extends AbstractDBFTestTable {
    public static Stream<Arguments> getVersions() {
        return DBFType.SUPPORTED_VERSIONS.stream().map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("getVersions")
    public void test(byte version) throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");
        DBFTableFile dbfTable = new DBFTableFile();
        dbfTable.getDbfHeader().setVersion(version);
        CharField charField = new CharField("FLD", 3);
        dbfTable.addField(charField);
        dbfTable.create(dbfFileName);

        charField.put("AAA");
        dbfTable.write();

        charField.put("BBB");
        dbfTable.write();

        charField.put("CCC");
        dbfTable.write();

        assertEquals(3, dbfTable.getRecordsCount());
        assertEquals(3, dbfTable.getCurrentRecordNumber());

        dbfTable.gotoFirstRecord();

        assertEquals("AAA", charField.get());
        dbfTable.close();
    }
}
