/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.MemoField;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DbfTest extends AbstractDBFTestTable {
    private static final String CHAR_FIELD_NAME = "char";
    private static final String MEMO_FIELD_NAME = "memo";

    public static Stream<Arguments> getMemoVersions() {
        return DBFType.SUPPORTED_MEMO_VERSIONS.stream().map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("getMemoVersions")
    public void testBuildDBF(byte dbfType) {
        String dbfFileName = getTempFileName("testdbt", ".dbf");

        DBFTableFile aDB = new DBFTableFile();
        aDB.getDbfHeader().setVersion(dbfType);

        CharField cf = new CharField(CHAR_FIELD_NAME, 10);
        MemoField mf = new MemoField(MEMO_FIELD_NAME);
        aDB.addField(cf);
        aDB.addField(mf);
        aDB.create(dbfFileName);

        aDB.close();

        aDB = new DBFTableFile();
        aDB.open(dbfFileName);

        cf = aDB.getField(CHAR_FIELD_NAME);
        mf = aDB.getField(MEMO_FIELD_NAME);
        cf.put("123456789");
        mf.put("123456789");

        aDB.write();

        cf.put("9");
        mf.put("9");

        aDB.write();

        aDB.close();

        aDB = new DBFTableFile(dbfFileName);

        cf = aDB.getField(CHAR_FIELD_NAME);
        mf = aDB.getField(MEMO_FIELD_NAME);

        aDB.read();

        String s = cf.get();
        assertEquals("123456789 ", s);

        s = mf.get();
        assertEquals("123456789", s);

        aDB.read();

        s = cf.get();
        assertEquals("9         ", s);

        s = mf.get();
        assertEquals("9", s);

        aDB.close();
    }
}
