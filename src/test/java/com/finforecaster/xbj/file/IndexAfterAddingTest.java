/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.fields.NumField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IndexAfterAddingTest extends AbstractDBFTestTable {
    private static String FIELD_NAME_ELMNO = "ElmNo";
    private static String FIELD_NAME_HITCOUNT = "HitCount";
    private static String FIELD_NAME_LSTDRWNO = "LstDrwNo";

    @Test
    public void testRun() throws Exception {
        final String dbfFileName = getTempFileName("testIndexAfterAdding", ".dbf");
        final String ndxFileName = getTempFileName("testIndexAfterAdding_elmno", ".ndx");

        NumField Elm_No = null;
        NumField Hit_Count = null;
        NumField Last_Draw_No = null;

        DBFTableFile oDB = new DBFTableFile();

        Elm_No = new NumField(FIELD_NAME_ELMNO, 2, 0);
        Hit_Count = new NumField(FIELD_NAME_HITCOUNT, 6, 0);
        Last_Draw_No = new NumField(FIELD_NAME_LSTDRWNO, 6, 0);

        oDB.addField(Elm_No);
        oDB.addField(Hit_Count);
        oDB.addField(Last_Draw_No);

        oDB.create(dbfFileName);
        oDB.createIndex(ndxFileName, FIELD_NAME_ELMNO, true, true);

        Elm_No.put(14);
        Hit_Count.put(22);
        Last_Draw_No.put(897);
        oDB.write();

        Elm_No.put(10);
        Hit_Count.put(3);
        Last_Draw_No.put(1);
        oDB.write();

        Elm_No.put(44);
        Hit_Count.put(33);
        Last_Draw_No.put(301);
        oDB.write();

        oDB.close();

        DBFTable pDB = new DBFTableFile(dbfFileName);
        Elm_No = pDB.getField(FIELD_NAME_ELMNO);
        Hit_Count = pDB.getField(FIELD_NAME_HITCOUNT);
        Last_Draw_No = pDB.getField(FIELD_NAME_LSTDRWNO);

        pDB.useIndex(ndxFileName);

        System.out.println("first");
        pDB.find("44");
        assertEquals("44", Elm_No.get());
        System.out.println("second");
        pDB.find("44");
        assertEquals("44", Elm_No.get());

        pDB.close();
    }
}
