/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.fields.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * test packing logic
 *
 * @author joseph mcverry
 */
public class PackTest extends AbstractDBFTestTable {

    public void build(boolean update, String dbfFileName, String ndxFileName0, String ndxFileName1) throws IOException, XBJException {
        //Create a new dbf file
        DBFTableFile aDB = new DBFTableFile();
        aDB.getDbfHeader().setVersion(DBFType.DBASEIII_WITH_MEMO);

        //Create the fields

        CharField classId = new CharField("classId", 9);
        CharField className = new CharField("className", 25);
        CharField teacherId = new CharField("teacherId", 9);
        CharField daysMeet = new CharField("daysMeet", 7);
        CharField timeMeet = new CharField("timeMeet", 4);
        NumField credits = new NumField("credits", 2, 0);
        LogicalField UnderGrad = new LogicalField("UnderGrad");
        MemoField discuss = new MemoField("discuss");

        //Add field definitions to database
        aDB.addField(classId);
        aDB.addField(className);
        aDB.addField(teacherId);
        aDB.addField(daysMeet);
        aDB.addField(timeMeet);
        aDB.addField(credits);
        aDB.addField(UnderGrad);
        aDB.addField(discuss);

        aDB.create(dbfFileName);
        aDB.createIndex(ndxFileName0, "classId", true, true);     //  true - delete ndx, true - unique index,
        aDB.createIndex(ndxFileName1, "teacherID+classId", true, false);     //true - delete NDX,  false - unique index,
        //System.out.println("index created");

        classId.put("JAVA10100");
        className.put("Introduction to JAVA");
        teacherId.put("120120120");
        daysMeet.put("NYNYNYN");
        timeMeet.put("0800");
        credits.put(3);
        UnderGrad.put(true);
        discuss.put("Intro class");

        aDB.write();

        classId.put("JAVA10200");
        className.put("Intermediate JAVA");
        teacherId.put("300020000");
        daysMeet.put("NYNYNYN");
        timeMeet.put("0930");
        credits.put(3);
        UnderGrad.put(true);
        discuss.put("itermediate class");

        aDB.write();

        classId.put("JAVA102D0");
        className.put("Interm");
        teacherId.put("300020000");
        daysMeet.put("ND");
        timeMeet.put("0930");
        credits.put(3);
        UnderGrad.put(true);
        discuss.put("itermediate class");

        aDB.write();

        if (!update) aDB.delete();

        classId.put("JAVA501");
        className.put("JAVA And Abstract Algebra");
        teacherId.put("120120120");
        daysMeet.put("NNYNYNN");
        timeMeet.put("0930");
        credits.put(6);
        UnderGrad.put(false);
        discuss.put("weird class");

        aDB.write();

        if (update == true) {
            aDB.gotoRecord(3);
            aDB.delete();
            aDB.update();
        }

        aDB.close();
    }

    @Test
    public void testPack() throws IOException, XBJException {
        String dbfFileName = getTempFileName("class", ".dbf");
        String ndxFileName0 = getTempFileName("class", ".ndx");
        String ndxFileName1 = getTempFileName("class", ".ndx");

        build(false, dbfFileName, ndxFileName0, ndxFileName1);
        DBFTable dbfTable = new DBFTableFile(dbfFileName);

        assertEquals(4, dbfTable.getRecordsCount());

        dbfTable.pack();

        assertEquals(3, dbfTable.getRecordsCount());

        for (int i = 1; i <= dbfTable.getRecordsCount(); i++) {
            dbfTable.gotoRecord(i);
            String bean = dbfTable.getField(1).get();
            if (i == 1)
                assertEquals("JAVA10100", bean);
            else if (i == 2)
                assertEquals("JAVA10200", bean);
            else
                assertEquals("JAVA501  ", bean);
        }

        dbfTable.close();
    }

    @Test
    public void testPackAfterUpdate() throws IOException {
        String dbfFileName = getTempFileName("class", ".dbf");
        String ndxFileName0 = getTempFileName("class", ".ndx");
        String ndxFileName1 = getTempFileName("class", ".ndx");

        build(false, dbfFileName, ndxFileName0, ndxFileName1);
        DBFTable dbfTable = new DBFTableFile(dbfFileName);

        assertEquals(4, dbfTable.getRecordsCount());

        dbfTable.pack();

        assertEquals(3, dbfTable.getRecordsCount());

        for (int i = 1; i < dbfTable.getRecordsCount(); i++) {
            dbfTable.gotoRecord(i);
            String bean = dbfTable.getField(1).get();
            if (i == 1)
                assertEquals("JAVA10100", bean);
            else if (i == 2)
                assertEquals("JAVA10200", bean);
            else
                assertEquals("JAVA501  ", bean);
        }

        dbfTable.close();
    }

    /*
     * If you delete every
record in a DBF, then call pack followed by a reindex, then attempt to
re-add a record which contains a prior unique key value, you will fail with
a duplicate key error. It appears the index doesn't get initialized when
reindex knows there are zero records on file.
     */
    @Test
    public void testBugDeleteAllPackReindexReadd() throws IOException {
        String dbfFileName = getTempFileName("class", ".dbf");
        String ndxFileName0 = getTempFileName("class", ".ndx");
        String ndxFileName1 = getTempFileName("class", ".ndx");

        build(false, dbfFileName, ndxFileName0, ndxFileName1);

        DBFTable aDB = new DBFTableFile(dbfFileName);
        aDB.useIndex(ndxFileName0);
        aDB.useIndex(ndxFileName1);


        for (int i = 0; i < aDB.getRecordsCount(); i++) {
            aDB.gotoRecord(i + 1);
            aDB.delete();
        }
        aDB.pack();
        aDB.getIndex(1).reIndex();
        aDB.getIndex(2).reIndex();
        Field classId = aDB.getField("classId");
        Field className = aDB.getField("className");
        Field teacherId = aDB.getField("teacherId");
        Field daysMeet = aDB.getField("daysMeet");
        Field timeMeet = aDB.getField("timeMeet");
        //Field credits = aDB.getField("credits");
        //Field UnderGrad = aDB.getField("UnderGrad");
        Field discuss = aDB.getField("discuss");

        classId.put("JAVA10100");
        className.put("Introduction to JAVA");
        teacherId.put("120120120");
        daysMeet.put("NYNYNYN");
        timeMeet.put("0800");
        discuss.put("Intro class");

        aDB.write();
        aDB.close();
    }
}
