/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.fields.CharField;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddTest extends AbstractDBFTestTable {
    @Test
    public void testAddFieldWithSameName() {
        final String dbfFileName = getTempFileName("test", ".dbf");

        DBFTableFile d1 = new DBFTableFile();
        CharField c = new CharField("C3", 10);
        d1.addField(c);
        d1.create(dbfFileName);
        d1.close();

        DBFTableFile d2 = new DBFTableFile();
        d2.open(dbfFileName);

        CharField c2 = new CharField("C3", 10);

        assertThrows(XBJException.class, () -> {
            d2.addField(c2);
        }, "shouldn't be able to add field again");

        d2.close();
    }
}
