/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import org.junit.jupiter.api.Test;
import com.finforecaster.xbj.XBJException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class DateFieldTest {
    @Test
    public void testDatePutCalendar() throws IOException, XBJException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2024, Calendar.DECEMBER, 16);
        DateField df = new DateField("test");
        df.put(calendar);
        assertEquals("20241216", df.get());
    }

    @Test
    public void testDatePutDate() throws IOException, XBJException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2024, Calendar.DECEMBER, 16);
        Date date = calendar.getTime();
        DateField df = new DateField("test");
        df.put(date);
        assertEquals("20241216", df.get());
    }

    @Test
    public void testEmptyDate() throws IOException {
        DateField dateField = new DateField("DF");
        assertNull(dateField.getLocalDate());
    }

    @Test
    public void testNullLocalDate() throws IOException {
        DateField dateField = new DateField("DF");
        assertDoesNotThrow(() -> dateField.put((LocalDate) null));
        assertEquals("        ", dateField.get());
    }
}
