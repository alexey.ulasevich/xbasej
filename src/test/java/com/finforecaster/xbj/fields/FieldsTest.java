/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.file.AbstractDBFTestTable;
import com.finforecaster.xbj.file.DBFTableFile;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FieldsTest extends AbstractDBFTestTable {
    @Test
    public void testPutString() throws XBJException, IOException {
        CharField f = new CharField("test", 10);
        f.put("a");
        assertEquals("a         ", f.get());
    }

    @Test
    public void testType() throws IOException, XBJException {
        CharField c = new CharField("C", 1);
        assertEquals('C', c.getType());
        DateField d = new DateField("D");
        assertEquals('D', d.getType());
        FloatField f = new FloatField("F", 10, 2);
        assertEquals('F', f.getType());
        NumField n = new NumField("N", 10, 2);
        assertEquals('N', n.getType());
        LogicalField l = new LogicalField("L");
        assertEquals('L', l.getType());
        PictureField p = new PictureField("P");
        assertEquals('P', p.getType());
        CurrencyField cc = new CurrencyField("Money");
        assertEquals('Y', cc.getType());
    }

    @Test
    public void testFloat() throws IOException, XBJException {
        String dbfFileName = getTempFileName("float", ".dbf");
        DBFTableFile db = new DBFTableFile();
        FloatField f = new FloatField("F", 10, 3);
        db.addField(f);
        db.create(dbfFileName);

        f.put(987.123f);
        db.write();
        db.close();

        db = new DBFTableFile();
        db.open(dbfFileName);
        f = db.getField("F");
        db.read();
        db.close();

        assertEquals("   987.123", f.get());
    }
}
