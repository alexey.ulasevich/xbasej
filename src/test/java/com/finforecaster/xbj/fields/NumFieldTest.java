/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumFieldTest {
    @Test
    public void testFieldCreatDefault() throws XBJException {
        NumField numField = new NumField("fieldName");

        assertEquals("fieldName", numField.getName());
        assertEquals('N', numField.getType());
        assertEquals(10, numField.getLength());
        assertEquals(0, numField.getDecimalPosition());
    }

    @Test
    public void testFieldCreate() throws XBJException, IOException {
        NumField numField = new NumField("fieldName", 12, 3);

        assertEquals("fieldName", numField.getName());
        assertEquals('N', numField.getType());
        assertEquals(12, numField.getLength());
        assertEquals(3, numField.getDecimalPosition());
    }

    @ParameterizedTest
    @MethodSource("testPutDoubleArguments")
    public void testPutDouble(double value, int length, int decPos, String result) throws IOException, XBJException {
        NumField numField = new NumField("name", length, decPos);
        numField.put(value);
        assertEquals(result, numField.get());
    }

    public static Stream<Arguments> testPutDoubleArguments() {
        return Stream.of(
                Arguments.of(7923.1236d, 9, 4, "7923.1236"),
                Arguments.of(7923.1236d, 9, 2, "  7923.12"),
                Arguments.of(-7923.1236d, 9, 2, " -7923.12"),
                Arguments.of(7923.1236d, 9, 0, "     7923"),
                Arguments.of(-7923.1236d, 9, 0, "    -7923"),
                Arguments.of(7923d, 9, 2, "  7923.00"),
                Arguments.of(-7923d, 9, 2, " -7923.00")
        );
    }

    @ParameterizedTest
    @MethodSource("testPutLongArguments")
    public void testPutLong(long value, int length, int decPos, String result) throws IOException, XBJException {
        NumField numField = new NumField("name", length, decPos);
        numField.put(value);
        assertEquals(result, numField.get());
    }

    public static Stream<Arguments> testPutLongArguments() {
        return Stream.of(
                Arguments.of(7923L, 9, 4, "7923.0000"),
                Arguments.of(7923L, 9, 2, "  7923.00"),
                Arguments.of(-7923L, 9, 2, " -7923.00"),
                Arguments.of(7923L, 9, 0, "     7923"),
                Arguments.of(-7923L, 9, 0, "    -7923"),
                Arguments.of(7923L, 9, 2, "  7923.00"),
                Arguments.of(-7923L, 9, 2, " -7923.00")
        );
    }

    @ParameterizedTest
    @MethodSource("testPutStringArguments")
    public void testPutString(String value, int length, int decPos, String result) throws IOException, XBJException {
        NumField numField = new NumField("name", length, decPos);
        numField.put(value);
        assertEquals(result, numField.get());
    }

    public static Stream<Arguments> testPutStringArguments() {
        return Stream.of(
                Arguments.of("7923.1236", 9, 4, "7923.1236"),
                Arguments.of("7923.1236", 9, 2, "  7923.12"),
                Arguments.of("-7923.1236", 9, 2, " -7923.12"),
                Arguments.of("7923.1236", 9, 0, "     7923"),
                Arguments.of("-7923.1236", 9, 0, "    -7923"),
                Arguments.of("7923", 9, 2, "  7923.00"),
                Arguments.of("-7923", 9, 2, " -7923.00")
        );
    }

    @Test
    public void testExceptionWhenNotFit() throws IOException, XBJException {
        NumField nf = new NumField("name", 6, 2);
        final double a = -50000000.36;
        assertThrows(XBJException.class, () -> nf.put(a));
    }

    @Test
    public void testExceptionWhenNonNumeric() throws IOException, XBJException {
        NumField nf = new NumField("name", 6, 2);
        assertThrows(XBJException.class, () -> nf.put("12MNa"));
    }

    @Test
    public void testNull() throws IOException, XBJException {
        NumField nf = new NumField("name", 6, 2);
        assertEquals(nf.get(), "      ");
    }

    @Test
    public void testGetDouble() throws IOException {
        NumField numField = new NumField("name", 10, 4);
        numField.put(3285.628d);

        assertEquals(3285.628d, numField.getDouble());
    }

    @Test
    public void testGetLong() throws IOException {
        NumField numField = new NumField("name");
        numField.put(3285L);

        assertEquals(3285L, numField.getLong());
    }
}
