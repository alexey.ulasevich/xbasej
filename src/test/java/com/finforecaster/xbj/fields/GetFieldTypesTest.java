/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetFieldTypesTest {

    @Test
    public void testCharField() throws Exception {
        CharField f = new CharField("a", 1);
        assertEquals(true, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testDateField() throws Exception {
        DateField f = new DateField("a");
        assertEquals(false, f.isCharField());
        assertEquals(true, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testFloatField() throws Exception {
        FloatField f = new FloatField("a", 10, 2);
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(true, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testLogicalField() throws Exception {
        LogicalField f = new LogicalField("a");
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(true, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testMemoField() throws Exception {
        MemoField f = new MemoField("a");
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(true, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testNumField() throws Exception {
        NumField f = new NumField("a", 10, 2);
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(true, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testPictureField() throws Exception {
        PictureField f = new PictureField("a");
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(true, f.isPictureField());
        assertEquals(false, f.isCurrencyFIeld());
    }

    @Test
    public void testCurrencyField() throws Exception {
        CurrencyField f = new CurrencyField("a");
        assertEquals(false, f.isCharField());
        assertEquals(false, f.isDateField());
        assertEquals(false, f.isFloatField());
        assertEquals(false, f.isLogicalField());
        assertEquals(false, f.isMemoField());
        assertEquals(false, f.isNumField());
        assertEquals(false, f.isPictureField());
        assertEquals(true, f.isCurrencyFIeld());
    }
}
