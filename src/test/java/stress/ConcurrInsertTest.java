/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package stress;

import com.finforecaster.xbj.fields.CharField;
import com.finforecaster.xbj.fields.Field;
import com.finforecaster.xbj.fields.NumField;
import com.finforecaster.xbj.file.AbstractDBFTestTable;
import com.finforecaster.xbj.file.DBFTableFile;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConcurrInsertTest extends AbstractDBFTestTable {
    private static final String FIELD_NAME_THREAD = "thread";
    private static final String FIELD_NAME_ROWNUM = "rownum";
    private static int ROWS_COUNT = 149;
    private static int THREAD_COUNT = 10;

    @Test
    public void testConcur() throws Exception {
        final String dbfFileName = getTempFileName("concurr", ".dbf");

        // create previously a dbf
        DBFTableFile writer = new DBFTableFile();
        Field str_field = new CharField(FIELD_NAME_THREAD, 15);
        Field int_field = new NumField(FIELD_NAME_ROWNUM, 5, 0);
        writer.addField(str_field);
        writer.addField(int_field);
        writer.create(dbfFileName);
        // add a row
        str_field.put("main thread");
        int_field.put("-1");
        writer.write();
        writer.close();

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        for (int threadIndex = 0; threadIndex < THREAD_COUNT; threadIndex++) {
            executorService.submit(new ConcurrInsert(threadIndex, dbfFileName));
        }

        executorService.shutdown();
        executorService.awaitTermination(60, TimeUnit.SECONDS);

        assertEquals(THREAD_COUNT * ROWS_COUNT + 1, new DBFTableFile(dbfFileName).getRecordsCount());
    }

    private class ConcurrInsert extends Thread {
        private int threadIndex = -1;
        private String dbfFileName;

        public ConcurrInsert(int threadIndex, String dbfFileName) throws Exception {
            this.dbfFileName = dbfFileName;
            this.threadIndex = threadIndex;
        }

        @Override
        public void run() {
            DBFTableFile writer = new DBFTableFile(dbfFileName);
            // ### thread adds new rows ###
            Field str_field = writer.getField(FIELD_NAME_THREAD);
            Field int_field = writer.getField(FIELD_NAME_ROWNUM);

            for (int i = 0; i < ROWS_COUNT; i++) {
                int_field.put(String.valueOf(i));
                str_field.put("Thread" + threadIndex);
                int_field.put("" + i);
                writer.write();
            }
            writer.close();
        }
    }
}
