/*
    xbj - Java access to dBase files
    Copyright 2023 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package stress;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.file.AbstractDBFTestTable;
import com.finforecaster.xbj.file.DBFTableFile;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RandomReadPerformanceTest extends AbstractDBFTestTable {
    private static final long ATTEMPTS = 1000000;

    @Test
    public void testRandomRead() throws IOException {
        String dbfFileName = getTempFileName("test", ".dbf");
        Files.copy(getClass().getResourceAsStream("/testfiles/KLADR.DBF"), Path.of(dbfFileName));
        DBFTable dbfTable = new DBFTableFile(dbfFileName);
        long start = System.currentTimeMillis();
        for (int i = 0; i < ATTEMPTS; i++) {
            int pos = (int) (Math.random() * dbfTable.getRecordsCount() - 1) + 1;
            dbfTable.gotoRecord(pos);
            dbfTable.read();
        }
        long stop = System.currentTimeMillis();
        long duration = stop - start;
        System.out.printf("duration %d ms\naverage %f ms/rec\n", duration, ((double) duration) / ATTEMPTS);
        assertTrue(duration < 7000);
        dbfTable.close();
    }
}
