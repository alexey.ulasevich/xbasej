/*
    xbj - Java access to dBase files
    Copyright 2023 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;

public class DBFHeader {
    private byte version = DBFType.DBASEIII;
    private byte lastUpdateDate[] = new byte[3];
    private int numberOfRecords = 0;
    private short lengthOfHeader = 32 + 1;
    private short lengthOfRecord = 1;
    private byte incompleteTransactionFlag = 0;
    private byte encryptFlag = 0;
    private byte reserve[] = new byte[12];
    private byte flagMDXexist = 0;
    private byte language = 0;
    private byte reserve2[] = new byte[2];

    public void read(RandomAccessFile file) {
        try {
            file.seek(0);
            version = file.readByte();

            file.read(lastUpdateDate, 0, 3);

            numberOfRecords = Util.x86(file.readInt());
            lengthOfHeader = Util.x86(file.readShort());
            lengthOfRecord = Util.x86(file.readShort());

            // skip reserved (2 bytes)
            file.readShort();

            incompleteTransactionFlag = file.readByte();
            encryptFlag = file.readByte();

            // skip reserved
            file.read(reserve, 0, 12);

            flagMDXexist = file.readByte();
            language = file.readByte();
            // skip reserved
            file.read(reserve2, 0, 2);
        } catch (IOException e) {
            throw new XBJException("XBJ0018: " + e.getMessage());
        }
    }

    public void write(RandomAccessFile file) {
        try {
            short currentrecord = 0;

            file.seek(0);

            Calendar d = Calendar.getInstance();

            if (d.get(Calendar.YEAR) < 2000)
                lastUpdateDate[0] = (byte) (d.get(Calendar.YEAR) - 1900);
            else
                lastUpdateDate[0] = (byte) (d.get(Calendar.YEAR) - 2000);

            lastUpdateDate[1] = (byte) (d.get(Calendar.MONTH) + 1);
            lastUpdateDate[2] = (byte) (d.get(Calendar.DAY_OF_MONTH));

            file.writeByte(version);
            file.write(lastUpdateDate, 0, 3);
            file.writeInt(Util.x86(numberOfRecords));
            file.writeShort(Util.x86(lengthOfHeader));
            file.writeShort(Util.x86(lengthOfRecord));
            file.writeShort(Util.x86(currentrecord));

            file.write(incompleteTransactionFlag);
            file.write(encryptFlag);
            file.write(reserve, 0, 12);
            file.write(flagMDXexist);
            file.write(language);
            file.write(reserve2, 0, 2);
        } catch (IOException exception) {
            throw new XBJException("XBJ0017: " + exception.getMessage());
        }
    }

    public short getLengthOfHeader() {
        return lengthOfHeader;
    }

    public byte getVersion() {
        return version;
    }

    public boolean isMDXExists() {
        return flagMDXexist == 0x01;
    }

    public void setMDXExist(boolean value) {
        flagMDXexist = (byte) (value ? 0x01 : 0x00);
    }

    public short getLengthOfRecord() {
        return lengthOfRecord;
    }

    public void setVersion(byte version) {
        if (!DBFType.SUPPORTED_VERSIONS.contains(version)) {
            throw new XBJException("XBJ0002: Invalid format specified");
        }

        this.version = version;
    }

    public void setLengthOfHeader(short lengthOfHeader) {
        this.lengthOfHeader = lengthOfHeader;
    }

    public void setLengthOfRecord(short lengthOfRecord) {
        this.lengthOfRecord = lengthOfRecord;
    }

    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public void setNumberOfRecords(int numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    public void setIncompleteTransactionFlag(byte incompleteTransactionFlag) {
        this.incompleteTransactionFlag = incompleteTransactionFlag;
    }

    public void setEncryptFlag(byte encryptFlag) {
        this.encryptFlag = encryptFlag;
    }

    public void setLanguage(byte language) {
        this.language = language;
    }

    public byte[] getReserve() {
        return reserve;
    }

    public void setReserve(byte[] reserve) {
        this.reserve = reserve;
    }

    public byte getLanguage() {
        return language;
    }

    public byte[] getReserve2() {
        return reserve2;
    }

    public void setReserve2(byte[] reserve2) {
        this.reserve2 = reserve2;
    }
}
