/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.indexes;

import java.util.Comparator;

/**
 * @author Joe McVerry - Raleigh NC USA
 *
 *         a comparator class for keylist objects
 */
public class KeyList implements Comparator<Object> {
	NodeKey value;
	int where;

	/**
	 *
	 */
	public KeyList(NodeKey v, int i) {
		value = v;
		where = i;
	}

	public int getWhere() {
		return where;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object arg0, Object arg1) {
		KeyList a = (KeyList) arg0;
		KeyList b = (KeyList) arg1;
		return a.value.compareKey(b.value);

	}
}
