/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj;

import com.finforecaster.xbj.fields.Field;
import com.finforecaster.xbj.indexes.Index;
import com.finforecaster.xbj.indexes.NDX;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Central abstract class of library. Used for work with dbf table. Parent class for any dbf table implementation.
 */
public abstract class DBFTable {
    public static final byte NOTDELETED = (byte) ' ';
    public static final byte DELETED = 0x2a;
    public final static String ENCODED_CHARSET = "8859_1";

    private int currentRecord = 0;
    private List<Field> fields = new ArrayList<>();
    private String keyString;

    private byte deletedFlag = (byte) ' ';

    private DBFHeader dbfHeader = new DBFHeader();

    private Index jNDX = null;
    private List<Index> jNDXes = new ArrayList<>();
    private List<String> jNDXID = new ArrayList<>();

    private ByteBuffer recordBuffer;

    private static Map<String, Lock> locks = new ConcurrentHashMap<>();

    /**
     * Default constructor
     */
    public DBFTable() {
    }

    /**
     * List of fields in the table
     *
     * @return list of fields
     */
    public List<Field> getFields() {
        return fields;
    }

    /**
     * Set new list of fields in the table, all fields will be replaced by new one
     *
     * @param fields list of fields
     */
    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    /**
     * Adds a new Field to a table
     *
     * @param field a predefined Field object
     * @return field
     */
    public <T extends Field> T addField(Field field) {
        validateItIsPossibleToAddNewField(field);
        getFields().add(field);
        getDbfHeader().setLengthOfRecord((short) (getFields().stream().mapToInt(Field::getLength).sum() + 1));
        getDbfHeader().setLengthOfHeader((short) (32 + getFields().size() * 32 + 1));
        return (T) field;
    }

    /**
     * Adds a fields to the table
     *
     * @param fields collection of predefined Field object
     */
    public void addFields(Collection<Field> fields) {
        for (Field field : fields) {
            addField(field);
        }
    }

    /**
     * Validate that it is possible to add new field to the table, if not an exception will be thrown.
     * The method can be overridden in child class.
     *
     * @param field field that we want to add
     */
    public void validateItIsPossibleToAddNewField(Field field) {
        if ((getDbfHeader().getVersion() == DBFType.DBASEIII && !getDbfHeader().isMDXExists()) || (getDbfHeader().getVersion() == DBFType.DBASEIII_WITH_MEMO)) {
            if (getFieldCount() == 128) {
                throw new XBJException("XBJ0005: Number of fields exceed limit of 128.");
            }
        } else {
            if (getFieldCount() == 255) {
                throw new XBJException("XBJ0006: Number of fields exceed limit of 255.");
            }
        }

        if (getFields().stream().map(Field::getName).anyMatch(existingName -> Objects.equals(existingName.toUpperCase(), field.getName().toUpperCase()))) {
            throw new XBJException("XBJ0007: Field with name '" + field.getName() + "' already exists in the table.");
        }

        if ((field.isMemoField() || field.isPictureField()) && !DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
            throw new XBJException("XBJ0020: Current table version does not support memo fields");
        }
    }

    /**
     * returns the number of fields in the table
     */
    public short getFieldCount() {
        return (short) fields.size();
    }

    /**
     * returns the number of records in the table (deleted records also counted)
     */
    public int getRecordsCount() {
        return getDbfHeader().getNumberOfRecords();
    }

    /**
     * returns the current record number
     */
    public int getCurrentRecordNumber() {
        return getCurrentRecord();
    }

    /**
     * returns the number of known index files and tags
     */
    public int getIndexCount() {
        return getjNDXes().size();
    }

    /**
     * gets an Index object associated with the database. This index does not become
     * the primary index. Written for the makeDBFBean application. Position is
     * relative to 1.
     *
     * @param indexPosition
     */
    public Index getIndex(int indexPosition) {
        if (indexPosition < 1)
            throw new XBJException("Index position too small");
        if (indexPosition > getjNDXes().size())
            throw new XBJException("Index position too large");
        return getjNDXes().get(indexPosition - 1);
    }

    /**
     * opens an Index file associated with the database. This index becomes the
     * primary index used in subsequent find methods.
     *
     * @param filename an existing ndx file(can be full or partial pathname) or mdx
     *                 tag
     */
    public Index useIndex(String filename) {
        try {
            for (Index NDXes : getjNDXes()) {
                if (NDXes.getName().compareTo(filename) == 0) {
                    setjNDX(NDXes);
                    return getjNDX();
                }
            }
            setjNDX(new NDX(filename, this, ' '));

            getjNDXes().add(getjNDX());
            return getjNDX();
        } catch (Exception exception) {
            throw new XBJException("XBJ0022: " + exception.getMessage());
        }
    }

    /**
     * opens an Index file associated with the database
     *
     * @param filename an existing Index file, can be full or partial pathname
     * @param ID       a unique id to define Index at run-time.
     */
    public Index useIndex(String filename, String ID) {
        useIndex(filename);
        getjNDXID().add(ID);
        return useIndex(filename);
    }

    /**
     * used to indicate the primary Index
     *
     * @param ndx an Index object
     */
    public Index useIndex(Index ndx) {

        for (Index NDXes : getjNDXes()) {
            if (NDXes == ndx) {
                setjNDX(NDXes);
                return NDXes;
            }
        }
        throw new XBJException("XBJ0023: Unknown Index " + ndx.getName());
    }

    /**
     * used to indicate the primary Index.
     *
     * @param ID String index name
     */
    public Index useIndexByID(String ID) {
        String NDXes;
        for (int i = 1; i <= getjNDXID().size(); i++) {
            NDXes = getjNDXID().get(i - 1);
            if (NDXes.compareTo(ID) == 0) {
                setjNDX(getjNDXes().get(i - 1));
                return getjNDXes().get(i - 1);
            }
        }
        throw new XBJException("XBJ0024: Unknown Index " + ID);
    }


    /**
     * creates a new Index as a NDX file, assumes NDX file does not exist.
     *
     * @param filename a new Index file name
     * @param index    string identifying Fields used in Index
     * @param unique   boolean to indicate if the key is always unique
     */
    public Index createIndex(String filename, String index, boolean unique) {
        return createIndex(filename, index, false, unique);
    }

    /**
     * creates a new Index as a NDX file.
     *
     * @param filename a new Index file name
     * @param index    string identifying Fields used in Index
     * @param destroy  permission to destory NDX if file exists
     * @param unique   boolean to indicate if the key is always unique
     */
    public Index createIndex(String filename, String index, boolean destroy, boolean unique) {
        try {
            setjNDX(new NDX(filename, index, this, destroy, unique));
            getjNDXes().add(getjNDX());
            return getjNDX();
        } catch (Exception exception) {
            throw new XBJException("XBJ0025: " + exception.getMessage());
        }
    }

    /**
     * used to find a record with an equal or greater string value. when done the
     * record pointer and field contents will be changed.
     *
     * @param keyString a search string
     * @return boolean indicating if the record found contains the exact key
     */
    public boolean find(String keyString) {
        try {
            this.keyString = keyString;
            if (getjNDX() == null)
                throw new XBJException("Index not defined");
            int r = getjNDX().find_entry(keyString);
            if (r < 1) {
                return false;
            }
            gotoRecord(r);
            return getjNDX().compareKey(keyString);
        } catch (Exception exception) {
            throw new XBJException("XBJ0026: " + exception.getMessage());
        }
    }

    /**
     * used to find a record with an equal and at the particular record. when done
     * the record pointer and field contents will be changed.
     *
     * @param keyString a search string
     * @param recno     - int record number
     * @return boolean indicating if the record found contains the exact key
     */
    public boolean find(String keyString, int recno) {
        try {
            if (getjNDX() == null)
                throw new XBJException("Index not defined");

            int r = getjNDX().find_entry(keyString, recno);
            if (r < 1)
                throw new XBJException("Record not found");

            gotoRecord(r);
            return getjNDX().compareKey(keyString);
        } catch (Exception exception) {
            throw new XBJException("XBJ0027: " + exception.getMessage());
        }
    }

    /**
     * used to find a record with an equal string value. when done the record
     * pointer and field contents will be changed only if the exact key is found.
     *
     * @param keyString a search string
     * @return boolean indicating if the record found contains the exact key
     */
    public boolean findExact(String keyString) {
        try {
            if (getjNDX() == null)
                throw new XBJException("Index not defined");
            int r = getjNDX().find_entry(keyString);
            if (r < 1)
                return false;

            if (getjNDX().didFindFindExact()) {
                gotoRecord(r);
            }

            return getjNDX().didFindFindExact();
        } catch (Exception exception) {
            throw new XBJException("XBJ0028: " + exception.getMessage());
        }
    }

    /**
     * used to get the next record in the index list. when done the record pointer
     * and field contents will be changed.
     *
     * @return true when record found, false when no more records
     */
    public boolean findNext() {
        try {
            if (getjNDX() == null) {
                throw new XBJException("Index not defined");
            }
            int r = getjNDX().get_next_key();
            if (r == -1) {
                // EOF riched
                return false;
            }
            gotoRecord(r);
            return getjNDX().compareKey(keyString);
        } catch (Exception exception) {
            throw new XBJException("XBJ0029: " + exception.getMessage());
        }
    }

    /**
     * used to get the previous record in the index list. when done the record
     * pointer and field contents will be changed.
     */
    public void findPrev() {
        try {
            if (getjNDX() == null)
                throw new XBJException("Index not defined");
            int r = getjNDX().get_prev_key();
            if (r == -1)
                throw new XBJException("Top Of File");

            gotoRecord(r);
        } catch (Exception exception) {
            throw new XBJException("XBJ0030: " + exception.getMessage());
        }
    }

    /**
     * used to read the next record, after the current record pointer, in the
     * database. when done the record pointer and field contents will be changed.
     */
    public void read() {
        if (getCurrentRecord() == getDbfHeader().getNumberOfRecords()) {
            throw new XBJException("XBJ0031: End Of File");
        }
        setCurrentRecord(getCurrentRecord() + 1);
        gotoRecord(getCurrentRecord());
    }

    /**
     * used to read the previous record, before the current record pointer, in the
     * database. when done the record pointer and field contents will be changed.
     */
    public void readPrev() {
        if (getCurrentRecord() < 1) {
            throw new XBJException("XBJ0032: Top Of File");
        }
        setCurrentRecord(getCurrentRecord() - 1);
        gotoRecord(getCurrentRecord());
    }

    /**
     * Used to read a record at a particular place in the database. when done the
     * record pointer and field contents will be changed.
     *
     * @param recno the relative position of the record to read
     */
    public abstract void gotoRecord(int recno);

    /**
     * Set current position to first record
     */
    public void gotoFirstRecord() {
        if (getRecordsCount() > 1) {
            gotoRecord(1);
        }
    }

    /**
     * used to position record pointer at the first record or index in the database.
     * when done the record pointer will be changed. NO RECORD IS READ. Your program
     * should follow this with either a read (for non-index reads) or findNext (for
     * index processing)
     */
    public void startTop() {
        try {
            if (getjNDX() == null)
                setCurrentRecord(0);
            else
                getjNDX().position_at_first();
        } catch (Exception exception) {
            throw new XBJException("XBJ0033: " + exception.getMessage());
        }
    }

    /**
     * used to position record pointer at the last record or index in the database.
     * when done the record pointer will be changed. NO RECORD IS READ. Your program
     * should follow this with either a read (for non-index reads) or findPrev (for
     * index processing)
     */
    public void startBottom() {
        try {
            if (getjNDX() == null)
                setCurrentRecord(getDbfHeader().getNumberOfRecords() + 1);
            else
                getjNDX().position_at_last();
        } catch (Exception exception) {
            throw new XBJException("XBJ0034: " + exception.getMessage());
        }
    }

    /**
     * Used to write a new record in the table, when done the record pointer is
     * at the end of the table.
     */
    public abstract void write();

    /**
     * Updates the record at the current position.
     */
    public abstract void update();

    /**
     * Marks the current records as deleted.
     */
    public abstract void delete();

    /**
     * Marks the current records as not deleted.
     */
    public abstract void undelete();

    /**
     * Closes the table.
     */
    public abstract void close();

    /**
     * Returns a Field object by its relative position.
     *
     * @param i Field number
     */
    public <T extends Field> T getField(int i) {
        if ((i < 1) || (i > getFieldCount())) {
            throw new XBJException("XBJ0035: Invalid Field number");
        }
        return (T) fields.get(i - 1);
    }

    /**
     * Returns a Field object by its name in the database.
     *
     * @param name Field name
     */
    public <T extends Field> T getField(String name) {
        short i;
        Field tField;

        for (i = 0; i < getFieldCount(); i++) {
            tField = fields.get(i);
            if (name.toUpperCase().compareTo(tField.getName().toUpperCase()) == 0) {
                return (T) tField;
            }
        }
        throw new XBJException("XBJ0036: Field not found " + name);
    }

    /**
     * Returns field of defined field class by field name
     *
     * @param name       field name
     * @param fieldClass field class
     * @param <T>        field class
     * @return field
     */
    public <T extends Field> T getField(String name, Class<T> fieldClass) {
        return getField(name);
    }

    /**
     * Returns field of defined field class by field id
     *
     * @param id         field id
     * @param fieldClass field class
     * @param <T>        field class
     * @return field
     */
    public <T extends Field> T getField(int id, Class<T> fieldClass) {
        return getField(id);
    }

    /**
     * Returns true if record is marked for deletion
     */
    public boolean isDeleted() {
        return (getDeletedFlag() == DELETED);
    }

    /**
     * Packs the table by removing deleted records and memo fields.
     */
    public abstract void pack();

    /**
     * Gets the character encoding string value.
     *
     * @return String "8859_1", "CP850", ..
     */
    public static String getEncodingType() {
        return ENCODED_CHARSET;
    }

    /**
     * Return current dbf table header data (@see DBFHeader)
     *
     * @return header data
     */
    public DBFHeader getDbfHeader() {
        return dbfHeader;
    }

    /**
     * Set new dbf header data
     *
     * @param dbfHeader
     */
    public void setDbfHeader(DBFHeader dbfHeader) {
        this.dbfHeader = dbfHeader;
    }

    /**
     * Return buffer used for stored record data
     *
     * @return
     */
    public ByteBuffer getRecordBuffer() {
        return recordBuffer;
    }

    /**
     * Set buffer used for stored record data
     *
     * @param recordBuffer
     */
    public void setRecordBuffer(ByteBuffer recordBuffer) {
        this.recordBuffer = recordBuffer;
    }

    /**
     * Get active current index
     *
     * @return
     */
    public Index getjNDX() {
        return jNDX;
    }

    /**
     * Set active current index
     *
     * @param jNDX
     */
    public void setjNDX(Index jNDX) {
        this.jNDX = jNDX;
    }

    /**
     * Return list of indexes
     *
     * @return
     */
    public List<Index> getjNDXes() {
        return jNDXes;
    }

    /**
     * Set list of indexes
     *
     * @param jNDXes
     */
    public void setjNDXes(List<Index> jNDXes) {
        this.jNDXes = jNDXes;
    }

    public List<String> getjNDXID() {
        return jNDXID;
    }

    public void setjNDXID(List<String> jNDXID) {
        this.jNDXID = jNDXID;
    }

    /**
     * Return current record number (first record has number 1)
     *
     * @return record number
     */
    public int getCurrentRecord() {
        return currentRecord;
    }

    /**
     * Set current record number (first record has number 1). Should not be used directly.
     *
     * @param currentRecord
     */
    public void setCurrentRecord(int currentRecord) {
        this.currentRecord = currentRecord;
    }

    /**
     * Return deleted flag (asterisk or space char). Asterisk when record marked as deleted.
     *
     * @return
     */
    public byte getDeletedFlag() {
        return deletedFlag;
    }

    /**
     * Set deleted flag (asterisk or space char). Asterisk when record marked as deleted.
     *
     * @param deletedFlag
     */
    public void setDeletedFlag(byte deletedFlag) {
        this.deletedFlag = deletedFlag;
    }

    /**
     * Return unique table id. It's used for synchronizing multithreading interactions with the table.
     * Have to unique for every table storage (file, memory or something else)
     *
     * @return
     */
    public abstract String getUniqueTableId();

    /**
     * Lock the table. If table locked by other thread will be waiting to unlock.
     */
    public synchronized void lock() {
        String tableId = getUniqueTableId();
        Lock lock = locks.get(tableId);
        if (lock == null) {
            lock = new ReentrantLock();
            locks.put(tableId, lock);
        }
        lock.lock();
    }

    /**
     * Unlock the table. It's recommended to use in finally part of try block.
     */
    public synchronized void unlock() {
        String tableId = getUniqueTableId();
        Lock lock = locks.get(tableId);
        lock.unlock();
    }
}
