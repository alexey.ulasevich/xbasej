/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj;

import java.util.Set;

public class DBFType {
    public static final byte UNKNOWN = 0x00;
    public static final byte FOXBASE = 0x02;
    public static final byte DBASEIII = 0x03;
    public static final byte DBASEIV = 0x04;
    public static final byte DBASEV = 0x05;

    public static final byte DBASEIII_WITH_MEMO = (byte) 0x83;
    public static final byte DBASEIV_WITH_MEMO = (byte) 0x8B;
    public static final byte FOXPRO_WITH_MEMO = (byte) 0xF5;

    public static final Set<Byte> SUPPORTED_VERSIONS = Set.of(DBASEIII, DBASEIV, DBASEIII_WITH_MEMO, DBASEIV_WITH_MEMO, FOXPRO_WITH_MEMO);
    public static final Set<Byte> SUPPORTED_MEMO_VERSIONS = Set.of(DBASEIII_WITH_MEMO, DBASEIV_WITH_MEMO, FOXPRO_WITH_MEMO);
}
