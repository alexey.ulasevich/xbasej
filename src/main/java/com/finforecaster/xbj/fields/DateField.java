/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.XBJException;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateField extends Field {
    public static final char type = 'D';

    /**
     * public method for creating a DateField object. It is not associated with a
     * database but can be when used with some DBF methods.
     *
     * @param iName the name of the field
     */
    public DateField(String iName) {
        super(iName);
        setLength(8);
        put("");
    }

    /**
     * return the character 'D' indicating a date field
     */
    @Override
    public char getType() {
        return type;
    }

    /**
     * sets field contents by a String parameter.
     *
     * @param inValue String value to store - format CCYYMMDD
     */
    @Override
    public void put(String inValue) {
        int i;
        boolean allspaces = true;

        for (i = 0; i < inValue.length(); i++) {
            if (inValue.charAt(i) != ' ')
                allspaces = false;
        }

        byte blankbyte = (byte) ' ';
        if (inValue.length() == 0 || allspaces) {
            for (i = 0; i < 8; i++) {
                buffer[i] = blankbyte;
            }
            return;
        }

        if (inValue.length() != 8)
            throw new XBJException("Invalid length for date Field");

        for (i = 0; i < 8; i++) {
            if (!Character.isDigit(inValue.charAt(i))) {
                throw new XBJException("Invalid format for date Field, " + inValue + " non numeric at position " + i);
            }
        }

        int yea = Integer.parseInt(inValue.substring(0, 4));
        int mo = Integer.parseInt(inValue.substring(4, 6));
        if (mo < 1 || mo > 12)
            throw new XBJException("Invalid format for date Field (month) " + inValue);
        int da = Integer.parseInt(inValue.substring(6, 8));
        if (da < 1)
            throw new XBJException("Invalid format for date Field (day) " + inValue);

        int month[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (yea == 2000 || (((yea % 4) == 0) && ((yea % 100) != 0)))
            month[2]++;
        if (da > month[mo])
            throw new XBJException("Invalid format for date Field, number of days > days in month");

        super.put(inValue);
    }

    /**
     * sets field contents by a Java Date object.
     *
     * @param inValue java.util.Date value to store
     */
    public void put(Date inValue) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        super.put(sdf.format(inValue));
    }

    /**
     * sets field contents by a Java Calendar object.
     *
     * @param inValue java.util.Calendare value to store
     */
    public void put(Calendar inValue) {
        put(inValue.getTime());
    }

    /**
     * sets field contents by a long value
     *
     * @param inValue long value to store - format CCYYMMDD
     */
    public void put(long inValue) {
        put(Long.toString(inValue));
    }

    /**
     * sets field contents by a Java LocalDate
     *
     * @param value date to store
     */
    public void put(LocalDate value) {
        put(value == null ? "" : value.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
    }

    /**
     * public method for returning the date field in a Java Calendar object.
     *
     * @return a Calendar object
     */
    public Calendar getCalendar() {
        Calendar getter = Calendar.getInstance();
        getter.set(Calendar.HOUR_OF_DAY, 0);
        getter.set(Calendar.MINUTE, 0);
        getter.set(Calendar.SECOND, 0);
        getter.set(Calendar.MILLISECOND, 0);
        getter.set(Calendar.YEAR, Integer.parseInt(get(Calendar.YEAR)));
        getter.set(Calendar.MONTH, Integer.parseInt(get(Calendar.MONTH)) - 1);
        getter.set(Calendar.DAY_OF_MONTH, Integer.parseInt(get(Calendar.DAY_OF_MONTH)));
        return getter;
    }

    public Calendar getCalendar(java.util.TimeZone timeZone) {
        Calendar result = Calendar.getInstance(timeZone);
        result.set(Calendar.HOUR_OF_DAY, 0);
        result.set(Calendar.MINUTE, 0);
        result.set(Calendar.SECOND, 0);
        result.set(Calendar.MILLISECOND, 0);
        result.set(Calendar.YEAR, Integer.parseInt(get(Calendar.YEAR)));
        result.set(Calendar.MONTH, Integer.parseInt(get(Calendar.MONTH)) - 1);
        result.set(Calendar.DAY_OF_MONTH, Integer.parseInt(get(Calendar.DAY_OF_MONTH)));
        return result;

    }

    /**
     * public method for returning the date field in a Java LocalDate object.
     *
     * @return a LocalDate object
     */
    public LocalDate getLocalDate() {
        String value = get();
        if (value.trim().isEmpty()) {
            return null;
        }
        return LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyyMMdd"));
    }


    /**
     * public method for getting individual field values
     *
     * @param field id, use Calendar.YEAR, Calendar.MONTh, Calendar.DAY_OF_MONTH
     * @return String of fields value
     */
    public String get(int field) {

        switch (field) {
            case Calendar.YEAR:
                return new String(buffer, 0, 4);
            case Calendar.MONTH:
                return new String(buffer, 4, 2);
            case Calendar.DAY_OF_MONTH:
                return new String(buffer, 6, 2);
            default:
                throw new XBJException("Field type invalid");
        }
    }

    /**
     * public method for setting individual field values
     *
     * @param field use Calendar.YEAR, Calendar.MONTh, Calendar.DAY_OF_MONTH
     * @param value - int value to set field
     */
    public void set(int field, int value) {
        NumberFormat numFormat;
        numFormat = NumberFormat.getNumberInstance();
        String setter;
        byte byter[];

        switch (field) {
            case Calendar.YEAR:
                numFormat.setMinimumIntegerDigits(4);
                numFormat.setMaximumIntegerDigits(4);
                setter = numFormat.format(value);
                try {
                    byter = setter.getBytes(DBFTable.getEncodingType());
                } catch (UnsupportedEncodingException UEE) {
                    byter = setter.getBytes();
                }
                buffer[0] = byter[0];
                buffer[1] = byter[1];
                buffer[2] = byter[2];
                buffer[3] = byter[3];
                break;
            case Calendar.MONTH:
                if (value < 1 || value > 12)
                    throw new XBJException("Month value out of range");
                numFormat.setMinimumIntegerDigits(2);
                numFormat.setMaximumIntegerDigits(2);
                setter = numFormat.format(value);
                byter = setter.getBytes();
                buffer[4] = byter[0];
                buffer[5] = byter[1];
                break;
            case Calendar.DAY_OF_MONTH:
                if (value < 1 || value > 31)
                    throw new XBJException("Day value out of range");
                numFormat.setMinimumIntegerDigits(2);
                numFormat.setMaximumIntegerDigits(2);
                setter = numFormat.format(value);
                byter = setter.getBytes();
                buffer[6] = byter[0];
                buffer[7] = byter[1];
                break;
            default:
                throw new XBJException("Field type invalid");
        }
    }
}
