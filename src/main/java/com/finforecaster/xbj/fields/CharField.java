/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

public class CharField extends Field {
    public static final char type = 'C';

    public CharField(String fieldName) {
        this(fieldName, 10);
    }

    /**
     * public method for creating a CharacterField object. It is not associated with
     * a database but can be when used with some DBF methods.
     *
     * @param iName   the name of the field
     * @param iLength length of Field, range 1 to 254 bytes
     */
    public CharField(String iName, int iLength) {
        super(iName);
        setLength(iLength);
        put("");
    }

    @Override
    public char getType() {
        return type;
    }
}
