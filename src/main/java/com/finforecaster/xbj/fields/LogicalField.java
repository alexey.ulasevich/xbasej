/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;

public class LogicalField extends Field {

    public final static byte BYTETRUE = (byte) 'T';
    public final static byte BYTEFALSE = (byte) 'F';
    public static final char type = 'L';

    /**
     * public method for creating a LogicalField object. It is not associated with a
     * database but can be when used with some DBF methods.
     *
     * @param fieldName the name of the field
     * @see Field
     */
    public LogicalField(String fieldName) {
        super(fieldName);
        setLength(1);
        put('F');
    }

    /**
     * return the character 'L' indicating a logical Field
     */
    @Override
    public char getType() {
        return type;
    }

    @Override
    public void put(String inValue) {
        String value = inValue.trim();

        if (value.length() == 0) {
            put(Boolean.FALSE);
            return;
        } else if (value.length() == 1) {
            put(value.charAt(0));
            return;
        }

        put(Boolean.valueOf(value));
    }

    /**
     * allows input of Y, y, T, t and 1 for true, N, n, F, f, and 0 for false
     */
    public void put(char inValue) {
        switch (inValue) {
            case 'Y':
            case 'y':
            case 'T':
            case 't':
            case '1':
                buffer[0] = BYTETRUE;
                break;
            case 'N':
            case 'n':
            case 'F':
            case 'f':
            case '0':
                buffer[0] = BYTEFALSE;
                break;
            default:
                throw new XBJException("XBJ0041: Invalid logical Field value");
        }
    }

    /**
     * allows input true or false
     */
    public void put(boolean inValue) {
        if (inValue)
            buffer[0] = BYTETRUE;
        else
            buffer[0] = BYTEFALSE;
    }

    /**
     * returns T for true and F for false
     */
    public char getChar() {
        return (char) buffer[0];
    }

    /**
     * returns true or false
     */
    public boolean getBoolean() {
        return ((buffer[0] == BYTETRUE));
    }
}
