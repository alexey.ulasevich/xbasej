/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;

import java.math.BigDecimal;
import java.math.BigInteger;

public class CurrencyField extends Field {

    public static final char type = 'Y';

    public CurrencyField(String iName) {
        super(iName);
        setLength(8);
    }

    @Override
    public char getType() {
        return type;
    }

    @Override
    public String get() {
        BigDecimal val = currencyFromByteArray(super.getBuffer());
        return new String(val.toString());
    }

    public void put(BigDecimal value) throws XBJException {
        super.put(currencyToByteArray(value));
    }

    private final BigDecimal currencyFromByteArray(byte[] bytes) {
        long d = (long) (bytes[7]) << 56 |
                /* long cast needed or shift done modulo 32 */
                (long) (bytes[6] & 0xff) << 48 | (long) (bytes[5] & 0xff) << 40 | (long) (bytes[4] & 0xff) << 32
                | (long) (bytes[3] & 0xff) << 24 | (long) (bytes[2] & 0xff) << 16 | (long) (bytes[1] & 0xff) << 8
                | bytes[0] & 0xff;
        return new BigDecimal(BigInteger.valueOf(d), 4);
    }

    private final byte[] currencyToByteArray(BigDecimal currencyValue) {
        byte[] bytes = new byte[8];
        long v = currencyValue.multiply(new BigDecimal(10000L)).longValue();
        bytes[0] = (byte) v;
        bytes[1] = (byte) (v >> 8);
        bytes[2] = (byte) (v >> 16);
        bytes[3] = (byte) (v >> 24);
        bytes[4] = (byte) (v >> 32);
        bytes[5] = (byte) (v >> 40);
        bytes[6] = (byte) (v >> 48);
        bytes[7] = (byte) (v >> 56);
        return bytes;
    }
}
