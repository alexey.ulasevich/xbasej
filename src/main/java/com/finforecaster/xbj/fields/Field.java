/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.file.MemoFile;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public abstract class Field {
    private String name;
    private int length = 0;

    protected byte[] buffer;

    public Field(String fieldName) {
        validateName(fieldName);
        setName(fieldName);
        setLength(length);
        Arrays.fill(buffer, (byte) 0x20);
    }

    public boolean isCharField() {
        return getType() == 'C';
    }

    public boolean isDateField() {
        return getType() == 'D';
    }

    public boolean isFloatField() {
        return getType() == 'F';
    }

    public boolean isMemoField() {
        return getType() == 'M';
    }

    public boolean isLogicalField() {
        return getType() == 'L';
    }

    public boolean isNumField() {
        return getType() == 'N';
    }

    public boolean isPictureField() {
        return getType() == 'P';
    }

    public boolean isCurrencyFIeld() {
        return getType() == 'Y';
    }

    private void validateName(String fieldName) throws XBJException {
        if ((fieldName == null) || (fieldName.length() == 0)) throw new XBJException("Missing field name");
        if (fieldName.length() > 10) throw new XBJException("Invalid field name " + fieldName);

        for (int i = 0; i < fieldName.length(); i++) {
            if (Character.isLetter(fieldName.charAt(i))) continue;
            if (Character.isDigit(fieldName.charAt(i))) continue;
            if (fieldName.charAt(i) == '_') continue;

            throw new XBJException("Invalid field name " + fieldName + ", character invalid at " + i);
        }
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public abstract char getType();

    /**
     * @return int - the number of decimal positions for numeric fields, zero
     * returned otherwise
     */
    public byte getDecimalPosition() {
        return 0;
    }

    public void read(ByteBuffer byteBuffer){
        byteBuffer.get(buffer);
    }

    /**
     * @return String field contents after any type of read.
     */
    public String get() {
        return new String(buffer);
    }

    /**
     * returns the original byte array as stored in the file.
     *
     * @return byte[] - may return a null if not set
     */
    public byte[] getBuffer() {
        return buffer;
    }

    public void write(ByteBuffer byteBuffer) {
        byteBuffer.put(buffer);
    }

    public void update(ByteBuffer byteBuffer) throws IOException, XBJException {
        byteBuffer.put(buffer);
    }

    /**
     * set field contents, no database updates until a DBF update or write is issued
     *
     * @param inValue value to set
     * @throws XBJException value length too long
     */
    public void put(String inValue) throws XBJException {
        byte b[] = inValue.getBytes();
        if (b.length > getLength()) {
            throw new XBJException("Value longer that field length");
        }

        Arrays.fill(buffer, (byte) 0x20);
        int count = Math.min(b.length, buffer.length);
        System.arraycopy(b, 0, buffer, 0, count);
    }

    /**
     * set field contents with binary data, no database updates until a DBF update
     * or write is issued if inValue is too short buffer is filled with binary
     * zeros.
     *
     * @param inValue byte array
     * @throws XBJException value length too long
     */
    public void put(byte inValue[]) throws XBJException {
        int i;

        if (inValue.length > getLength()) throw new XBJException("Field length too long");

        for (i = 0; i < inValue.length; i++)
            buffer[i] = inValue[i];

        for (; i < getLength(); i++)
            buffer[i] = ' ';
    }

    @Override
    public String toString() {
        return this.getName() + "; " + this.getType() + "; " + this.getLength() + "; " + this.getDecimalPosition();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(int length) {
        this.length = length;
        buffer = new byte[length];
        Arrays.fill(buffer, (byte) 0x20);
    }

    public void writeHeader(RandomAccessFile file, byte version) throws IOException, XBJException {
        byte[] byter = new byte[15];
        int nameLength = getName().length();
        int i = 0;
        byte b[];
        try {
            b = getName().toUpperCase().getBytes(DBFTable.ENCODED_CHARSET);
        } catch (UnsupportedEncodingException UEE) {
            b = getName().toUpperCase().getBytes();
        }
        for (int x = 0; x < b.length; x++)
            byter[x] = b[x];

        file.write(byter, 0, nameLength);

        for (i = 0; i < 14; i++)
            byter[i] = 0;

        file.writeByte(0);
        if (nameLength < 10)
            file.write(byter, 0, 10 - nameLength);

        file.writeByte(getType());

        file.write(byter, 0, 4);

        if (isCharField() && getLength() > 256) {
            file.writeByte(getLength() % 256);
            file.writeByte(getLength() / 256);
        } else {
            file.writeByte(getLength());
            file.writeByte(getDecimalPosition());
        }

        if (version == DBFType.DBASEIII || version == DBFType.DBASEIII_WITH_MEMO)
            byter[2] = 1;

        file.write(byter, 0, 14);
    }

    public static Field readFieldHeader(RandomAccessFile file, MemoFile dbtFile) throws IOException, XBJException {
        Field tField;
        int i;
        byte[] byter = new byte[15];
        String name;
        char type;
        byte length;
        int iLength;
        int decpoint;

        file.readFully(byter, 0, 11);
        for (i = 0; i < 12 && byter[i] != 0; i++)
            ;
        try {
            name = new String(byter, 0, i, DBFTable.ENCODED_CHARSET);
        } catch (UnsupportedEncodingException UEE) {
            name = new String(byter, 0, i);
        }

        type = (char) file.readByte();

        file.readFully(byter, 0, 4);

        length = file.readByte();
        if (length > 0)
            iLength = length;
        else
            iLength = 256 + length;
        decpoint = file.readByte();

        file.readFully(byter, 0, 14);

        switch (type) {
            case CharField.type:
                if (decpoint > 0)
                    iLength += decpoint * 256;
                tField = new CharField(name, iLength);
                break;
            case DateField.type:
                tField = new DateField(name);
                break;
            case FloatField.type:
                tField = new FloatField(name, iLength, decpoint);
                break;
            case LogicalField.type:
                tField = new LogicalField(name);
                break;
            case MemoField.type:
                tField = new MemoField(name, dbtFile);
                break;
            case NumField.type:
                tField = new NumField(name, iLength, decpoint);
                break;
            case PictureField.type:
                tField = new PictureField(name, dbtFile);
                break;
            case CurrencyField.type:
                tField = new CurrencyField(name);
                break;
            default:
                throw new XBJException(String.format("XBJ0021: Unknown Field type '%H' for %s", (byte) type, name));
        }
        return tField;
    }
}
