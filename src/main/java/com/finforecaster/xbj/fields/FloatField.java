/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - 	Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;

import java.io.IOException;

public class FloatField extends NumField {
    public static final char type = 'F';

    public FloatField(String named, int len, int dec) throws IOException, XBJException {
        super(named, len, dec);
    }

    @Override
    public char getType() {
        return type;
    }
}
