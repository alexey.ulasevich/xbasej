/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.XBJException;

import java.util.Formatter;
import java.util.Locale;

public class NumField extends Field {
    private byte decPosition = 0;

    public static final char type = 'N';

    /**
     * public method for creating a numeric field object. It is not associated with
     * a database but can be when used with some DBF methods.
     *
     * @param fieldName the name of the field
     */
    public NumField(String fieldName) {
        super(fieldName);
        setLength(10);
        setDecimalPosition((byte) 0);
    }

    /**
     * public method for creating a numeric field object. It is not associated with
     * a database but can be when used with some DBF methods.
     *
     * @param iName         the name of the field
     * @param iLength       the length of Field. range is 1 to 19 bytes
     * @param inDecPosition the number of decimal positions range from 2 to 17
     *                      bytes. Relative to Length.
     */
    public NumField(String iName, int iLength, int inDecPosition) {
        this(iName);
        setLength(iLength);
        setDecimalPosition((byte) inDecPosition);
    }

    /**
     * return the character 'N' indicating a numeric field
     */
    @Override
    public char getType() {
        return type;
    }

    /**
     * @return int - the number of decimal positions for numeric fields, zero
     * returned otherwise
     */
    @Override
    public byte getDecimalPosition() {
        return decPosition;
    }

    public void setDecimalPosition(byte dec) {
        decPosition = dec;
    }

    /**
     * sets the field contents.
     *
     * @param value double
     */
    public void put(double value) {
        super.put(new Formatter(Locale.US).format("%" + getLength() + "." + decPosition + "f", value).toString());
    }

    /**
     * set field contents, no database updates until a DBF update or write is issued
     *
     * @param value string represent of numeric value
     */
    @Override
    public void put(String value) {
        try {
            put(Double.parseDouble(value));
        } catch (NumberFormatException exception) {
            throw new XBJException(exception.getMessage());
        }
    }

    /**
     * Return field content as double value
     *
     * @return value
     */
    public double getDouble() {
        return Double.parseDouble(get().trim());
    }

    /**
     * Return field content as long value
     *
     * @return value
     */
    public long getLong() {
        return Long.parseLong(get().trim());
    }
}
