/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.file.MemoFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class MemoField extends Field {
    boolean foxPro = false;
    public static final char type = 'M';

    private MemoFile memoFile;
    private int originalSize;
    private String value;
    private byte[] byteValue;

    public boolean isFoxPro() {
        return foxPro;
    }

    public void setMemoFile(MemoFile indbtobj) {
        memoFile = indbtobj;
    }

    public MemoField(String Name, MemoFile indbtobj) {
        this(Name);
        setLength(10);
        memoFile = indbtobj;
        value = new String("");
    }

    /**
     * public method for creating a memo field object. It is not associated with a
     * database but can be when used with some DBF methods.
     *
     * @param fieldName the name of the field
     */
    public MemoField(String fieldName) {
        super(fieldName);
        setLength(10);
        memoFile = null;
        originalSize = 0;
        buffer = new byte[10];
        Arrays.fill(buffer, MemoFile.BYTEZERO);
        value = new String("");
    }

    /**
     * public method for creating a FoxPro memo field object. It is not associated
     * with a database but can be when used with some DBF methods.
     *
     * @param iName    the name of the field
     * @param inFoxPro - boolean
     */
    public MemoField(String iName, boolean inFoxPro)  {
        this(iName);
        foxPro = inFoxPro;
        memoFile = null;
        originalSize = 0;
        buffer = new byte[10];
        for (int i = 0; i < 10; i++)
            buffer[i] = MemoFile.BYTEZERO;
        value = new String("");
    }

    /**
     * return the character 'M' indicating a memo field
     */
    @Override
    public char getType() {
        return type;
    }

    /**
     * return the contents of the memo Field, variant of the field.get method
     */
    @Override
    public String get() {
        String s = "";
        if (byteValue == null)
            return "";
        try {
            s = new String(byteValue, DBFTable.getEncodingType());
        } catch (UnsupportedEncodingException UEE) {
            s = new String(byteValue);
        }
        int k;
        if (byteValue.length < 2)
            return s;
        for (k = byteValue.length; k > -1 && byteValue[k - 1] == 0; k--)
            ;
        return s.substring(0, k);
    }

    /**
     * return the contents of the memo Field via its original byte array
     *
     * @return byte[] - if not set a null is returned.
     */
    @Override
    public byte[] getBuffer() {
        return byteValue;
    }

    @Override
    public void read(ByteBuffer byteBuffer) {
        super.read(byteBuffer);
        try {
            byteValue = memoFile.readBytes(super.buffer);
            value = "";
            originalSize = 0;

            if (byteValue != null) {
                value = new String(byteValue);
                originalSize = value.length();
            }
        } catch (IOException exception) {
            throw new XBJException("XBJ0042: " + exception.getMessage());
        }
    }

    /**
     * sets the contents of the memo Field, variant of the field.put method data not
     * written into DBF until an update or write is issued.
     *
     * @param invalue value to set Field to.
     */
    @Override
    public void put(String invalue) {
        value = new String(invalue);
        byteValue = value.getBytes();
    }

    /**
     * sets the contents of the memo Field, variant of the field.put method data not
     * written into DBF until an update or write is issued.
     *
     * @param inBytes byte array value to set Field to.
     */
    @Override
    public void put(byte inBytes[]) {
        byteValue = inBytes;
    }

    @Override
    public void write(ByteBuffer byteBuffer) {
        super.buffer = memoFile.write(value, originalSize, true, super.buffer);
        super.write(byteBuffer);
    }

    @Override
    public void update(ByteBuffer byteBuffer) {
        super.buffer = memoFile.write(value, originalSize, false, super.buffer);
        super.write(byteBuffer);
    }
}
