/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.fields;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.file.MemoFile;
import com.finforecaster.xbj.file.MemoFileFXP;
import com.finforecaster.xbj.XBJException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class PictureField extends Field {
    private MemoFileFXP memoFile;
    private int originalSize;
    private String value;
    private byte[] byteValue;
    public static final char type = 'P';

    public void setMemoFile(MemoFile memoFile) {
        this.memoFile = (MemoFileFXP) memoFile;
    }

    public PictureField(String fieldName, MemoFile memoFile) {
        this(fieldName);
        this.memoFile = (MemoFileFXP) memoFile;
        value = new String("");
    }

    /**
     * method for creating a picture field object. It is not associated with
     * a database but can be when used with some DBF methods.
     *
     * @param fieldName the name of the field
     */
    public PictureField(String fieldName) {
        super(fieldName);
        setLength(10);
        memoFile = null;
        originalSize = 0;
        buffer = new byte[10];
        for (int i = 0; i < 10; i++)
            buffer[i] = MemoFile.BYTEZERO;
        value = new String("");
    }

    /**
     * return the character 'P' indicating a picture field
     */
    @Override
    public char getType() {
        return type;
    }

    /**
     * return the contents of the picture Field, variant of the field.get method
     */
    @Override
    public String get() {
        if (byteValue == null)
            return "";
        try {
            return new String(byteValue, DBFTable.getEncodingType());
        } catch (UnsupportedEncodingException UEE) {
            return new String(byteValue);
        }
    }

    /**
     * return the contents of the picture Field via its original byte array
     *
     * @return byte[] - if not set a null is returned.
     */
    @Override
    public byte[] getBuffer() {
        return byteValue;
    }

    @Override
    public void read(ByteBuffer byteBuffer) {
        super.read(byteBuffer);
        try {
            byteValue = memoFile.readBytes(super.buffer);
            if (byteValue == null)
                originalSize = 0;
            else
                originalSize = value.length();
        } catch (Exception exception) {
            throw new XBJException("XBJ0043: " + exception.getMessage());
        }
    }

    /**
     * sets the contents of the picture Field, variant of the field.put method data
     * not written into DBF until an update or write is issued.
     *
     * @param invalue value to set Field to.
     */
    @Override
    public void put(String invalue) {
        throw new XBJException("XBJ0044: use put(Bytes[])");
    }

    /**
     * sets the contents of the picture Field, variant of the field.put method data
     * not written into DBF until an update or write is issued.
     *
     * @param inBytes value to set Field to.
     */
    @Override
    public void put(byte inBytes[]) throws XBJException {
        byteValue = inBytes;
    }

    @Override
    public void write(ByteBuffer byteBuffer) {
        super.buffer = memoFile.write(byteValue, originalSize, true, super.buffer);
        super.write(byteBuffer);
    }

    @Override
    public void update(ByteBuffer byteBuffer) throws IOException, XBJException {
        super.buffer = memoFile.write(byteValue, originalSize, false, super.buffer);
        super.write(byteBuffer);
    }
}
