/*
    xbj - Java access to dBase files
    Copyright 2024 - Alexey Ulasevich, Moscow, Russia

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.DBFType;
import com.finforecaster.xbj.XBJException;
import com.finforecaster.xbj.fields.Field;
import com.finforecaster.xbj.fields.MemoField;
import com.finforecaster.xbj.indexes.Index;
import com.finforecaster.xbj.indexes.MDXFile;
import com.finforecaster.xbj.indexes.NDX;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class DBFTableFile extends DBFTable {
    public static final byte END_OF_DBF_HEADER = 0x0D;

    private File dbfFile;
    private RandomAccessFile raDbfFile;
    private MemoFile memoFile = null;
    private MDXFile mdxFile = null;
    private FileChannel channel = null;
    private boolean dbfFileCreated = false;

    /**
     * Creates a table object. No any files will be created or opened till call 'create' or 'open' methods.
     */
    public DBFTableFile() {
    }

    /**
     * Creates table object and opens an existing dbf-file.
     *
     * @param dbfTableFileName
     */
    public DBFTableFile(String dbfTableFileName) {
        open(dbfTableFileName);
    }

    /**
     * Opens an existing table file.
     *
     * @param dbfTableFileName an existing table file name, can be full or partial pathname
     */
    public void open(String dbfTableFileName) {
        try {
            setDbfFile(new File(dbfTableFileName));
            lock();
            setRaDbfFile(new RandomAccessFile(dbfTableFileName, "rw"));
            channel = getRaDbfFile().getChannel();

            getDbfHeader().read(getRaDbfFile());
            setRecordBuffer(ByteBuffer.allocateDirect(getDbfHeader().getLengthOfRecord() + 1));

            if (!DBFType.SUPPORTED_VERSIONS.contains(getDbfHeader().getVersion())) {
                throw new XBJException("XBJ0001: Version id " + getDbfHeader().getVersion() + " is not supported");
            }

            if (DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
                openMemoFile();
            }

            getFields().clear();
            short fieldsCount = (short) ((getDbfHeader().getLengthOfHeader() - 32) / 32);
            for (int i = 0; i < fieldsCount; i++) {
                Field field = Field.readFieldHeader(getRaDbfFile(), getMemoFile());
                getFields().add(field);
            }

            if (getDbfHeader().isMDXExists()) {
                mdxFile = new MDXFile(getFileName(), this, ' ');
                for (int i = 0; i < mdxFile.getAnchor().getIndexes(); i++) getjNDXes().add(mdxFile.MDXes[i]);
            }

            setCurrentRecord(0);
            setDbfFileCreated(true);
        } catch (IOException exception) {
            throw new XBJException("XBJ0016: " + exception.getMessage());
        } finally {
            unlock();
        }
    }

    /**
     * Opens memo file (dbt/fpt file) for the table
     */
    private void openMemoFile() {
        try {
            setMemoFile(switch (getDbfHeader().getVersion()) {
                case DBFType.FOXPRO_WITH_MEMO -> new MemoFileFXP(this);
                case DBFType.DBASEIII_WITH_MEMO -> new MemoFileDBIII(this);
                case DBFType.DBASEIV_WITH_MEMO -> new MemoFileDBIV(this);
                default -> null;
            });
        } catch (IOException exception) {
            throw new XBJException("XBJ0004: " + exception.getMessage());
        }
    }

    /**
     * Creates a table file (dbf file).
     *
     * @param dbfFileName
     */
    public void create(String dbfFileName) {
        try {
            setDbfFile(new File(dbfFileName));
            lock();
            setRaDbfFile(new RandomAccessFile(dbfFileName, "rw"));
            channel = getRaDbfFile().getChannel();

            setRecordBuffer(ByteBuffer.allocateDirect(getDbfHeader().getLengthOfRecord() + 1));

            if (getDbfHeader().getVersion() == DBFType.DBASEIV || getDbfHeader().getVersion() == DBFType.DBASEIV_WITH_MEMO) {
                getDbfHeader().setMDXExist(true);
            }

            getDbfHeader().write(getRaDbfFile());
            for (Field field : getFields()) {
                field.writeHeader(getRaDbfFile(), getDbfHeader().getVersion());
            }

            getRaDbfFile().writeByte(END_OF_DBF_HEADER);

            if (DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
                createMemoFile();
            }

            if (getDbfHeader().isMDXExists()) {
                mdxFile = new MDXFile(dbfFileName, this);
            }
            setDbfFileCreated(true);
        } catch (Exception exception) {
            throw new XBJException("XBJ0019: " + exception.getMessage());
        } finally {
            unlock();
        }
    }

    /**
     * Creates memo file
     */
    private void createMemoFile() {
        try {
            String memoFileName = getFileName().substring(0, getFileName().length() - 4);
            setMemoFile(switch (getDbfHeader().getVersion()) {
                case DBFType.FOXPRO_WITH_MEMO -> new MemoFileFXP(this, memoFileName + ".fpt", true);
                case DBFType.DBASEIII_WITH_MEMO -> new MemoFileDBIII(this, memoFileName + ".dbt", true);
                case DBFType.DBASEIV_WITH_MEMO -> new MemoFileDBIV(this, memoFileName + ".dbt", true);
                default -> null;
            });
            for (Field field : getFields()) {
                if (field.isMemoField()) {
                    ((MemoField) field).setMemoFile(getMemoFile());
                }
            }
        } catch (Exception exception) {
            throw new XBJException("XBJ0003: " + exception.getMessage());
        }
    }

    @Override
    public void validateItIsPossibleToAddNewField(Field field) {
        super.validateItIsPossibleToAddNewField(field);
        if (isDbfFileCreated()) {
            throw new XBJException("XBJ0015: It's impossible to add new field, because table already saved.");
        }
    }

    /**
     * Returns a file object that used to store table data
     *
     * @return file
     */
    public RandomAccessFile getRaDbfFile() {
        return raDbfFile;
    }

    /**
     * Associates all Index operations with an existing tag.
     *
     * @param tagname an existing tag name in the production MDX file
     */
    public Index useTag(String tagname) {
        if (mdxFile == null) throw new XBJException("No MDX file associated with this database");
        setjNDX(mdxFile.getMDX(tagname));
        return getjNDX();
    }

    /**
     * Associates all Index operations with an existing tag.
     *
     * @param tagname an existing tag name in the production MDX file
     * @param ID      a unique id to define Index at run-time.
     */
    public Index useTag(String tagname, String ID) {
        useTag(tagname);
        getjNDXID().add(ID);

        return useTag(tagname);
    }

    /**
     * Creates a tag in the MDX file.
     *
     * @param tagname  a non-existing tag name in the production MDX file
     * @param tagIndex string identifying Fields used in Index
     * @param unique   boolean to indicate if the key is always unique
     */
    public Index createTag(String tagname, String tagIndex, boolean unique) {
        if (mdxFile == null) throw new XBJException("XBJ0037: No MDX file associated with this database");
        try {
            setjNDX(mdxFile.createTag(tagname, tagIndex, unique));
            getjNDXes().add(getjNDX());
            return getjNDX();
        } catch (Exception exception) {
            throw new XBJException("XBJ0038: " + exception.getMessage());
        }
    }

    @Override
    public void gotoRecord(int recno) {
        // TODO: move something to parent
        try {
            lock();
            if ((recno > getDbfHeader().getNumberOfRecords()) || (recno < 1)) {
                throw new XBJException("XBJ0039: Invalid Record Number " + recno);
            }
            setCurrentRecord(recno);

            seek(recno - 1);
            getRecordBuffer().clear();

            channel.read(getRecordBuffer());

            getRecordBuffer().rewind();

            setDeletedFlag(getRecordBuffer().get());
            for (Field field : getFields()) {
                field.read(getRecordBuffer());
            }

            for (Index NDXes : getjNDXes()) {
                NDXes.set_active_key(NDXes.build_key());
            }
        } catch (IOException exception) {
            throw new XBJException("XBJ0008: " + exception.getMessage());
        } finally {
            unlock();
        }
    }

    @Override
    public void write() {
        try {
            lock();
            for (Index NDXes : getjNDXes()) {
                NDXes.check_for_duplicates(Index.findFirstMatchingKey);
            }
            getDbfHeader().read(getRaDbfFile());

            seek(getDbfHeader().getNumberOfRecords());

            setDeletedFlag(NOTDELETED);
            getRecordBuffer().position(0);
            getRecordBuffer().put(getDeletedFlag());

            for (Field field : getFields()) {
                field.write(getRecordBuffer());
            }

            getRecordBuffer().position(0);
            channel.write(getRecordBuffer());

            byte wb = 0x1a;
            getRaDbfFile().writeByte(wb);

            if (!getDbfHeader().isMDXExists() && (getDbfHeader().getVersion() == DBFType.DBASEIII || getDbfHeader().getVersion() == DBFType.DBASEIII_WITH_MEMO)) {
                getRecordBuffer().position(0);
                channel.write(getRecordBuffer());
                wb = ' ';
                for (int i = 0; i < getDbfHeader().getLengthOfRecord(); i++)
                    getRaDbfFile().writeByte(wb);
            }

            for (Index NDXes : getjNDXes()) {
                NDXes.add_entry(getDbfHeader().getNumberOfRecords() + 1);
            }

            getDbfHeader().setNumberOfRecords(getDbfHeader().getNumberOfRecords() + 1);
            getDbfHeader().write(getRaDbfFile());
            setCurrentRecord(getDbfHeader().getNumberOfRecords());
        } catch (IOException exception) {
            throw new XBJException("XBJ0009: " + exception.getMessage());
        } finally {
            unlock();
        }
    }

    @Override
    public void update() {
        int i;
        Field tField;

        if ((getCurrentRecord() < 1) || (getCurrentRecord() > getDbfHeader().getNumberOfRecords())) {
            throw new XBJException("Invalid current record pointer");
        }

        try {
            seek(getCurrentRecord() - 1);

            getRecordBuffer().position(0);
            getRecordBuffer().put(getDeletedFlag());

            for (Index NDXes : getjNDXes()) {
                NDXes.check_for_duplicates(getCurrentRecord());
            }

            for (Index NDXes : getjNDXes()) {
                NDXes.find_entry(NDXes.get_active_key(), getCurrentRecord());
            }

            for (i = 0; i < getFieldCount(); i++) {
                tField = getFields().get(i);
                if (tField.isMemoField())
                    tField.update(getRecordBuffer());
                else
                    tField.write(getRecordBuffer());
            }

            getRecordBuffer().position(0);
            channel.write(getRecordBuffer());

            for (Index NDXes : getjNDXes()) {
                NDXes.update(getCurrentRecord());
            }
        } catch (IOException exception) {
            throw new XBJException("XBJ0010: " + exception.getMessage());
        }
    }

    private void seek(long recno) {
        long calcpos = getDbfHeader().getLengthOfHeader() + (getDbfHeader().getLengthOfRecord() * recno);
        try {
            getRaDbfFile().seek(calcpos);
        } catch (IOException e) {
            throw new XBJException("XBJ0040: " + e.getMessage());
        }
    }

    @Override
    public void delete() {
        try {
            seek(getCurrentRecord() - 1);
            setDeletedFlag(DELETED);
            getRaDbfFile().writeByte(getDeletedFlag());
        } catch (IOException exception) {
            throw new XBJException("XBJ0011: " + exception.getMessage());
        }
    }

    @Override
    public void undelete() {
        try {
            seek(getCurrentRecord() - 1);
            setDeletedFlag(NOTDELETED);
            getRaDbfFile().writeByte(getDeletedFlag());
        } catch (IOException exception) {
            throw new XBJException("XBJ0012:" + exception.getMessage());
        }
    }

    @Override
    public void close() {
        short i;

        try {
            if (getMemoFile() != null)
                getMemoFile().close();

            Index NDXes;
            NDX n;

            if (getjNDXes() != null) {
                for (i = 1; i <= getjNDXes().size(); i++) {
                    NDXes = getjNDXes().get(i - 1);
                    if (NDXes instanceof NDX) {
                        n = (NDX) NDXes;
                        n.close();
                    }
                }
            }

            if (mdxFile != null) mdxFile.close();

            setMemoFile(null);
            setjNDXes(new ArrayList<>());
            mdxFile = null;
            channel.close();
            getRaDbfFile().close();
        } catch (IOException exception) {
            throw new XBJException("XBJ0013: " + exception.getMessage());
        }
    }

    /**
     * Returns the full path name of the table file
     */
    public String getFileName() {
        return getDbfFile().getAbsolutePath();
    }

    @Override
    public void pack() {
        try {
            Index index = getjNDX();
            List<Index> indices = getjNDXes();
            List<String> ids = getjNDXID();

            String memoFileName = null;
            String dbfFileName = getFileName();

            String tempMemoFileName = null;
            String tempDbfFileName = dbfFileName.substring(0, dbfFileName.length() - 4) + "_pack.dbf";

            RandomAccessFile tempRAFile = new RandomAccessFile(tempDbfFileName, "rw");
            byte[] fullHeader = new byte[getDbfHeader().getLengthOfHeader()];
            getRaDbfFile().seek(0);
            getRaDbfFile().read(fullHeader);
            // set number of records to 0
            fullHeader[4] = 0x00;
            fullHeader[5] = 0x00;
            fullHeader[6] = 0x00;
            fullHeader[7] = 0x00;
            tempRAFile.write(fullHeader);
            tempRAFile.close();

            if (DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
                File mFile = getMemoFile().getFile();
                memoFileName = mFile.getAbsolutePath();
                String tempMemoFileNameExt = memoFileName.substring(memoFileName.length() - 4, memoFileName.length());
                tempMemoFileName = memoFileName.substring(0, (memoFileName.length() - 4)) + "_pack" + tempMemoFileNameExt;

                RandomAccessFile tempRaMemoFile = new RandomAccessFile(tempMemoFileName, "rw");

                byte[] memoHeader = new byte[512];

                getMemoFile().getRaFile().seek(0);
                getMemoFile().getRaFile().read(memoHeader);
                // make temp memo file empty
                memoHeader[0] = 0x00;
                memoHeader[1] = 0x00;
                memoHeader[2] = 0x00;
                memoHeader[3] = 0x00;
                tempRaMemoFile.write(memoHeader);
            }

            DBFTableFile dbfTableFile = new DBFTableFile(tempDbfFileName);
            for (int i = 1; i <= getRecordsCount(); i++) {
                gotoRecord(i);
                if (!isDeleted()) {
                    for(short fieldId = 1; fieldId <= getFieldCount(); fieldId++) {
                        Field srcField = getField(fieldId);
                        Field trgField = dbfTableFile.getField(fieldId);
                        trgField.put(srcField.getBuffer());
                    }
                    dbfTableFile.write();
                }
            }
            dbfTableFile.close();


            close();

            Files.copy(Path.of(tempDbfFileName), Path.of(dbfFileName), REPLACE_EXISTING);
            if (DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
                Files.copy(Path.of(tempMemoFileName), Path.of(memoFileName), REPLACE_EXISTING);
            }

            open(dbfFileName);
            setjNDX(index);
            setjNDXes(indices);
            setjNDXID(ids);

            Files.deleteIfExists(Path.of(tempDbfFileName));
            if (DBFType.SUPPORTED_MEMO_VERSIONS.contains(getDbfHeader().getVersion())) {
                Files.deleteIfExists(dbfTableFile.getDbfFile().toPath());
            }
        } catch (IOException exception) {
            throw new XBJException("XBJ0014: " + exception.getMessage());
        }
    }

    @Override
    public String getUniqueTableId() {
        return getFileName();
    }

    public boolean isDbfFileCreated() {
        return dbfFileCreated;
    }

    public void setDbfFileCreated(boolean dbfFileCreated) {
        this.dbfFileCreated = dbfFileCreated;
    }

    public MemoFile getMemoFile() {
        return memoFile;
    }

    public void setMemoFile(MemoFile memoFile) {
        this.memoFile = memoFile;
    }

    public File getDbfFile() {
        return dbfFile;
    }

    public void setDbfFile(File dbfFile) {
        this.dbfFile = dbfFile;
    }

    public void setRaDbfFile(RandomAccessFile raDbfFile) {
        this.raDbfFile = raDbfFile;
    }
}
