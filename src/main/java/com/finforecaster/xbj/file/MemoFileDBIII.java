/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.Util;
import com.finforecaster.xbj.XBJException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MemoFileDBIII extends MemoFile {

    public MemoFileDBIII(DBFTableFile dbfTable) throws IOException, XBJException {
        super(dbfTable, createMemoFileName(dbfTable, "dbt"));
    }

    public MemoFileDBIII(DBFTableFile dbfTable, String name, boolean destroy) throws IOException, XBJException {
        super(dbfTable, name, destroy, createMemoFileName(dbfTable, "dbt"));
    }

    @Override
    public void setNextBlock() throws IOException {
        if (getRaFile().length() == 0) {
            getRaFile().writeInt(Util.x86(1));
            nextBlock = 1;
            getRaFile().seek(511);
            getRaFile().writeByte(0);
        } else {
            nextBlock = Util.x86(getRaFile().readInt());
        }
    }

    @Override
    public byte[] readBytes(byte[] input) throws IOException, XBJException {

        byte[] bTemp = new byte[513];
        boolean work = true;
        boolean onefound = false;
        byte[] bTemp2 = null;
        byte[] bTemp3 = null;
        int workLength = 0;

        for (int i = 0; i < 10; i++) {
            if (input[i] >= BYTEZERO && input[i] <= '9')
                break;
            input[i] = BYTEZERO;
        }

        String sPos;
        sPos = new String(input, 0, 10);
        long lPos = Long.parseLong(sPos);
        if (lPos == 0)
            return null;
        getRaFile().seek(lPos * memoBlockSize);
        int i;

        do {
            getRaFile().read(bTemp, 0, memoBlockSize);
            for (i = 0; i < memoBlockSize; i++) {
                if (bTemp[i] == 0x1a) {
                    if (onefound) {
                        work = false;
                        bTemp[i] = 0;
                        i--;
                        break;
                    }
                    work = false;
                    onefound = true;
                    break;
                } else if (bTemp[i] == 0x00) {
                    if (onefound) {
                        work = false;
                        break;
                    }
                    onefound = false;
                } else
                    onefound = false;
            }
            if (workLength > 0) {
                bTemp3 = new byte[workLength];
                System.arraycopy(bTemp2, 0, bTemp3, 0, workLength);
            }
            bTemp2 = new byte[workLength + i];
            if (workLength > 0)
                System.arraycopy(bTemp3, 0, bTemp2, 0, workLength);
            System.arraycopy(bTemp, 0, bTemp2, workLength, i);
            workLength += i;

            if (workLength > getRaFile().length())
                throw new XBJException("XB0048: error reading dbt file, reading exceeds length of file");
        } while (work);
        return bTemp2;
    }

    @Override
    public byte[] write(String value, int originalSize, boolean write, byte originalPos[]) {
        try {
            boolean madebigger;
            long startPos;
            int pos;
            byte buffer[] = new byte[512];

            if (value.length() == 0) {
                byte breturn[] = {BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO, BYTEZERO,
                        BYTEZERO};
                return breturn;
            }

            if ((originalSize == 0) && (value.length() > 0))
                madebigger = true;
            else if (((value.length() / memoBlockSize) + 1) > ((originalSize / memoBlockSize) + 1))
                madebigger = true;
            else
                madebigger = false;

            if (madebigger || write) {
                startPos = nextBlock;
                nextBlock += ((value.length() + 2) / memoBlockSize) + 1;
            } else {
                String sPos;
                sPos = new String(originalPos, 0, 10);
                startPos = Long.parseLong(sPos);
            } /* endif */

            getRaFile().seek(startPos * memoBlockSize);

            for (pos = 0; pos < value.length(); pos += memoBlockSize) {
                byte b[];
                if ((pos + memoBlockSize) > value.length()) {
                    try {
                        b = value.substring(pos, value.length()).getBytes(DBFTable.getEncodingType());
                    } catch (UnsupportedEncodingException UEE) {
                        b = value.substring(pos, value.length()).getBytes();
                    }
                } else {
                    try {
                        b = value.substring(pos, (pos + memoBlockSize)).getBytes(DBFTable.getEncodingType());
                    } catch (UnsupportedEncodingException UEE) {
                        b = value.substring(pos, (pos + memoBlockSize)).getBytes();
                    }
                }

                for (int x = 0; x < b.length; x++)
                    buffer[x] = b[x];

                getRaFile().write(buffer, 0, 512);
            } /* endfor */

            getRaFile().seek((startPos * memoBlockSize) + value.length());
            getRaFile().writeByte(26);
            getRaFile().writeByte(26);

            if (madebigger || write) {
                getRaFile().seek((memoBlockSize * nextBlock) - 1);
                getRaFile().writeByte(26);
                getRaFile().seek(0);
                getRaFile().writeInt(Util.x86(nextBlock));
            }

            String returnString = new String(Long.toString(startPos));

            byte ten[] = new byte[10];
            byte newTen[] = new byte[10];
            newTen = returnString.getBytes();

            for (pos = 0; pos < (10 - returnString.length()); pos++)
                ten[pos] = BYTEZERO;

            for (int x = 0; pos < 10; pos++, x++)
                ten[pos] = newTen[x];

            return ten;
        } catch (Exception exception) {
            throw new XBJException("XBJ0047: " + exception.getMessage());
        }
    }
}
