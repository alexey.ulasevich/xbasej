/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.XBJException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class MemoFile {
    private RandomAccessFile raFile;
    private File file;
    public int memoBlockSize = 512;
    public int nextBlock;
    public DBFTable database;
    public final static byte BYTEZERO = (byte) '0';
    public final static byte BYTESPACE = (byte) ' ';

    public String extension = "dbt";

    public MemoFile(DBFTableFile iDBFTable, String memoFileName) throws IOException {
        database = iDBFTable;
        setFile(new File(memoFileName));
        if (!getFile().exists() || !getFile().isFile()) {
            throw new XBJException("XBJ0048: Can't find Memo Text file " + memoFileName);
        }
        setRaFile(new RandomAccessFile(memoFileName, "rw"));
        setNextBlock();
    }

    public MemoFile(DBFTable iDBFTable, String name, boolean destroy, String memoFileName) throws IOException, XBJException {
        database = iDBFTable;
        setFile(new File(memoFileName));

        if (!destroy)
            if (getFile().exists())
                throw new XBJException("Memeo Text File exists, can't destroy");

        if (destroy)
            if (getFile().exists())
                if (!getFile().delete())
                    throw new XBJException("Can't delete old Memo Text file");

        FileOutputStream tFOS = new FileOutputStream(getFile());
        tFOS.close();
        setRaFile(new RandomAccessFile(getFile(), "rw"));
        setNextBlock();
    }

    public abstract void setNextBlock() throws IOException;

    public abstract byte[] readBytes(byte[] input) throws IOException, XBJException;

    public abstract byte[] write(String value, int originalSize, boolean write, byte originalPos[]);

    public void seek(int pos) throws IOException {
        long lpos = pos;
        getRaFile().seek(lpos);
    }

    public void close() throws IOException {
        getRaFile().close();
    }

    public RandomAccessFile getRaFile() {
        return raFile;
    }

    public void setRaFile(RandomAccessFile raFile) {
        this.raFile = raFile;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public static String createMemoFileName(DBFTableFile dbfTableFile, String extension) {
        return dbfTableFile.getFileName().substring(0, dbfTableFile.getFileName().length() - 3) + extension;
    }
}
