/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.XBJException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MemoFileFXP extends MemoFile {
    int blocks = 0;
    int memoBlockSize;

    public MemoFileFXP(DBFTableFile dbfTable) throws IOException, XBJException {
        super(dbfTable, createMemoFileName(dbfTable, "fpt"));
        nextBlock = getRaFile().readInt();
        memoBlockSize = getRaFile().readInt();
    }

    public MemoFileFXP(DBFTableFile dbfTable, String name, boolean destroy) throws IOException, XBJException {
        super(dbfTable, name, destroy, createMemoFileName(dbfTable, "fpt"));
        nextBlock = 8;
        getRaFile().writeInt(nextBlock);
        getRaFile().writeByte(0);
        getRaFile().writeByte(0);
        memoBlockSize = 64;
        getRaFile().writeShort(memoBlockSize);
        for (int i = 0; i < 504; i += 4)
            getRaFile().writeInt(0);
    }

    @Override
    public void setNextBlock() throws IOException {

    }

    @Override
    public byte[] readBytes(byte[] input) throws IOException, XBJException {

        int i;
        for (i = 0; i < 10; i++) {
            if (input[i] >= BYTEZERO && input[i] <= '9')
                break;
            if (input[i] == BYTESPACE)
                input[i] = BYTEZERO;
        }

        String sPos = new String(input, 0, 10);

        for (i = 0; i < sPos.length(); i++)
            if (sPos.charAt(i) != BYTESPACE)
                break;
        if (i == sPos.length())
            return null;

        int lPos = Integer.parseInt(sPos.trim());

        if (lPos == 0)
            return null;
        long longpos = lPos;
        getRaFile().seek((longpos * memoBlockSize));

        int orisize;

        orisize = 0;

        getRaFile().skipBytes(4); /* [ 1985813 ] Bug in DBT_fpt.java */
        int size = getRaFile().readInt();

        orisize = size;

        byte work_buffer[] = new byte[orisize];
        getRaFile().read(work_buffer, 0, orisize);

        return work_buffer;

    }

    @Override
    public byte[] write(String value, int originalSize, boolean write, byte originalPos[]) {
        try {
            return write(value.getBytes(DBFTable.getEncodingType()), originalSize, write, originalPos);
        } catch (UnsupportedEncodingException UEE) {
            return write(value.getBytes(), originalSize, write, originalPos);
        }
    }

    public byte[] write(byte inBytes[], int originalSize, boolean write, byte originalPos[]) {
        try {
            int pos, startPos;
            int length;
            boolean madebigger = false;

            if (inBytes.length == 0) {
                byte breturn[] = {BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE,
                        BYTESPACE, BYTESPACE};
                return breturn;
            }

            if ((originalSize == 0) && (inBytes.length > 0))
                madebigger = true;
            else if (((inBytes.length / memoBlockSize) + 1) > ((originalSize / memoBlockSize) + 1))
                madebigger = true;
            else
                madebigger = false;

            if (madebigger || write) {
                startPos = nextBlock;
                nextBlock += ((inBytes.length + 2) / memoBlockSize) + 1;

                getRaFile().seek(0);
                getRaFile().writeInt(nextBlock);
            } else {
                String sPos;
                sPos = new String(originalPos, 0, 10);
                startPos = Integer.parseInt(sPos.trim());

            } /* endif */

            length = inBytes.length;

            pos = (length / memoBlockSize) + 1;

            long longpos = startPos;
            getRaFile().seek((longpos * memoBlockSize));

            int inType = 1;
            getRaFile().writeInt(inType);
            getRaFile().writeInt(length);

            byte buffer[] = inBytes;
            getRaFile().write(buffer, 0, length);

            length = memoBlockSize - ((length) % memoBlockSize);

            if (length < memoBlockSize)
                while (length-- > 0)
                    getRaFile().write(0);

            String returnString = new String(Long.toString(startPos));

            byte ten[] = new byte[10];

            for (pos = 0; pos < (10 - returnString.length()); pos++)
                ten[pos] = BYTEZERO;

            byte b[];
            b = returnString.getBytes();
            for (int x = 0; x < b.length; x++, pos++)
                ten[pos] = b[x];

            return ten;
        } catch (Exception exception) {
            throw new XBJException("XBJ0045: " + exception.getMessage());
        }
    }

}
