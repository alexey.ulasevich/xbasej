/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj.file;

import com.finforecaster.xbj.DBFTable;
import com.finforecaster.xbj.Util;
import com.finforecaster.xbj.XBJException;

import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MemoFileDBIV extends MemoFile {

    static int LAST_IND = 0x8ffff;
    int blocks = 0;

    public MemoFileDBIV(DBFTableFile dbfTable) throws IOException, XBJException {
        super(dbfTable, createMemoFileName(dbfTable, "dbt"));
        nextBlock = Util.x86(getRaFile().readInt());
        getRaFile().skipBytes(16);
        memoBlockSize = Util.x86(getRaFile().readInt());
    }

    public MemoFileDBIV(DBFTableFile dbfTable, String name, boolean destroy) throws IOException, XBJException {
        super(dbfTable, name, destroy, createMemoFileName(dbfTable, "dbt"));
        nextBlock = 1;
        getRaFile().writeInt(Util.x86(nextBlock));
        for (int i = 0; i < 16; i++)
            getRaFile().writeByte(0);
        memoBlockSize = 512;
        getRaFile().writeInt(Util.x86(memoBlockSize));
    }

    @Override
    public void setNextBlock() throws IOException {

    }

    @Override
    public byte[] readBytes(byte[] input) throws IOException, XBJException {
        for (int i = 0; i < 10; i++) {
            if (input[i] == 0)
                input[i] = BYTESPACE;
        }
        String sPos = new String(input, 0, 10).trim();
        if (sPos.length() == 0)
            return null;
        long lPos = Long.parseLong(sPos.trim());
        if ((lPos == 0) || (lPos * memoBlockSize >= getRaFile().length()))
            return null;

        getRaFile().seek(lPos * memoBlockSize);

        int orisize;

        orisize = 0;

        int lastind = Util.x86(getRaFile().readInt());

        if (lastind != LAST_IND)
            throw new XBJException("Unexpected encounter in read text file");

        int size = Util.x86(getRaFile().readInt());

        orisize = size - 8;

        byte work_buffer[] = new byte[orisize + 1];
        getRaFile().read(work_buffer, 0, orisize);

        work_buffer[orisize] = (byte) '\0';

        return work_buffer;

    }

    @Override
    public byte[] write(String value, int originalSize, boolean write, byte originalPos[]) {
        try {
            int pos, startPos;
            int nextavail = 0, bytes_blocks_used, last_stop, next_stop, lastused = 0;
            int length;
            boolean eof = false;
            boolean madebigger = false;

            if (value.length() == 0) {
                byte breturn[] = {BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE, BYTESPACE,
                        BYTESPACE, BYTESPACE};
                return breturn;
            }

            if ((originalSize == 0) && (value.length() > 0))
                madebigger = true;
            else if (((value.length() / memoBlockSize) + 1) > ((originalSize / memoBlockSize) + 1))
                madebigger = true;
            else
                madebigger = false;

            if (madebigger || write) {
                startPos = nextBlock;
                nextBlock += ((value.length() + 2) / memoBlockSize) + 1;
                lastused = 0;
            } else {
                String sPos;
                sPos = new String(originalPos, 0, 10);
                startPos = Integer.parseInt(sPos.trim());
                lastused = startPos;
            } /* endif */

            length = value.length();

            length += 8;
            pos = (length / memoBlockSize) + 1;

            last_stop = next_stop = 0;
            while (true) {
                try {
                    long longnextstop = next_stop;
                    getRaFile().seek(longnextstop * memoBlockSize);
                    nextavail = Util.x86(getRaFile().readInt());
                } catch (EOFException ioe) {
                    eof = true;
                    break;
                }

                if (nextavail == LAST_IND)
                    throw new XBJException("Error while writing to memo file, unexpected encounter");

                bytes_blocks_used = Util.x86(getRaFile().readInt());

                if (pos <= bytes_blocks_used) {
                    long longnextstop = next_stop;
                    getRaFile().seek(longnextstop * memoBlockSize);
                    break;
                }
                last_stop = next_stop;
                next_stop = nextavail;
            } /* endwhile */

            getRaFile().writeInt(Util.x86(LAST_IND));
            getRaFile().writeInt(Util.x86(length));

            length -= 8;
            byte buffer[];
            try {
                buffer = value.getBytes(DBFTable.getEncodingType());
            } catch (UnsupportedEncodingException UEE) {
                buffer = value.getBytes();
            }

            getRaFile().write(buffer, 0, length);

            if (eof || lastused == 0)
                nextavail += pos;

            if (eof) {
                long longnextavail = nextavail;
                getRaFile().seek(longnextavail * memoBlockSize - 1);
                getRaFile().write(0);
            }
            if (lastused == 0) { // writting a record don't update old record
                long longlaststop = last_stop;
                getRaFile().seek(longlaststop * memoBlockSize);
                getRaFile().writeInt(Util.x86(nextavail));
            } else {
                long longlastused = lastused;
                getRaFile().seek(longlastused * memoBlockSize);
                getRaFile().writeInt(Util.x86(nextavail));
                bytes_blocks_used = Util.x86(getRaFile().readInt());
                getRaFile().seek(lastused * memoBlockSize + 4);
                bytes_blocks_used /= memoBlockSize;
                bytes_blocks_used++;
                getRaFile().writeInt(Util.x86(bytes_blocks_used));
                long longlaststop = last_stop;
                getRaFile().seek(longlaststop * memoBlockSize);
                getRaFile().writeInt(Util.x86(lastused));
            } /* endif */

            String returnString = new String(Long.toString(next_stop));

            byte ten[] = new byte[10];

            for (pos = 0; pos < (10 - returnString.length()); pos++)
                ten[pos] = BYTEZERO;

            byte b[];
            b = returnString.getBytes();
            for (int x = 0; x < b.length; x++, pos++)
                ten[pos] = b[x];

            return ten;
        } catch (Exception exception) {
            throw new XBJException("XBJ0046: " + exception.getMessage());
        }
    }
}
