/*
    xbj - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbj;

public class Util extends Object {
    private static boolean x86Architecture = ((System.getProperty("os.arch").indexOf("86") == 0)
            && (System.getProperty("os.arch").compareTo("vax") != 0));

    public static long x86(long in) {
        if (x86Architecture)
            return in;

        boolean negative = false;
        long is;
        long first, second, third, fourth, fifth, sixth, seventh, eigth, isnt, save;
        save = in;
        if (in < 0) {
            negative = true;
            // in = ((byte) in & 0x7fffffffffffffff);
        }
        first = in >>> 56;
        if (negative)
            first = (byte) in & 0x7f;
        if (negative)
            first |= 0x80;
        isnt = first << 56;
        save = in - isnt;
        second = save >>> 48;
        isnt = second << 48;
        save = save - isnt;
        third = save >>> 40;
        isnt = third << 40;
        save = save - isnt;
        fourth = save >>> 32;
        isnt = fourth << 32;
        save = save - isnt;
        fifth = save >>> 24;
        isnt = fifth << 24;
        save = save - isnt;
        sixth = save >>> 16;
        isnt = sixth << 16;
        save = save - isnt;
        seventh = save >>> 8;
        isnt = seventh << 8;
        save = save - isnt;
        eigth = save; // - seventh;
        is = (eigth << 56) + (seventh << 48) + (sixth << 40) + (fifth << 32) + (fourth << 24) + (third << 16)
                + (second << 8) + first;
        return is;
    }

    public static int x86(int in) {
        if (x86Architecture) {
            return in;
        }
        boolean negative = false;
        int is;
        int first, second, third, fourth, save;
        save = in;
        if (in < 0) {
            negative = true;
            in &= 0x7fffffff;
        }
        first = in >>> 24;
        if (negative)
            first |= 0x80;
        in = save & 0x00ff0000;
        second = in >>> 16;
        in = save & 0x0000ff00;
        third = in >>> 8;
        fourth = save & 0x000000ff;
        is = (fourth << 24) + (third << 16) + (second << 8) + first;
        return is;
    }

    public static short x86(short in) {
        if (x86Architecture)
            return in;
        short is, save = in;
        boolean negative = false;
        int first, second;
        if (in < 0) {
            negative = true;
            in &= 0x7fff;
        }
        first = in >>> 8;
        if (negative)
            first |= 0x80;
        second = save & 0x00ff;
        is = (short) ((second << 8) + first);
        return is;
    }


    // TODO: make it simple - use something like Date.getTime()
    public static double doubleDate(String s) {
        int i;

        if (s.trim().length() == 0)
            return 1e100;

        int year = Integer.parseInt(s.substring(0, 4));
        if (year == 0)
            return 1e100;

        int days[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        int month = Integer.parseInt(s.substring(4, 6));
        int day = Integer.parseInt(s.substring(6, 8));

        int daydif = 2378497;

        if ((year / 4) == 0)
            days[2] = 29;

        if (year > 1799) {
            daydif += day - 1;
            for (i = 2; i <= month; i++)
                daydif += days[i - 1];
            daydif += (year - 1800) * 365;
            daydif += ((year - 1800) / 4);
            daydif -= ((year - 1800) % 100); // leap years don't occur in 00
            // years
            if (year > 1999) // except in 2000
                daydif++;
        } else {
            daydif -= (days[month] - day + 1);
            for (i = 11; i >= month; i--)
                daydif -= days[i + 1];
            daydif -= (1799 - year) * 365;
            daydif -= (1799 - year) / 4;
        }

        Integer retInt = daydif;

        return retInt.doubleValue();
    }
}
